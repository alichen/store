package main

import (
	"github.com/astaxie/beego"
	"store/controllers/home"
)

func main() {
	beego.Router("/", &home.MainController{})
	beego.Router("/sidebar", &home.SidebarController{})

	beego.AutoRouter(&home.ContainerController{})
	beego.Router("/container/update/:id", &home.ContainerController{}, "*:Update")
	beego.Router("/container/view/:id", &home.ContainerController{}, "*:View")
	beego.Router("/container/delete/:id", &home.ContainerController{}, "Get:Delete")
	beego.AutoRouter(&home.TypeController{})
	beego.Router("/type/update/:id", &home.TypeController{}, "*:Update")
	beego.Router("/type/view/:id", &home.TypeController{}, "*:View")
	beego.Router("/type/delete/:id", &home.TypeController{}, "Get:Delete")
	beego.AutoRouter(&home.TypeController{})
	beego.Router("/type/update/:id", &home.TypeController{}, "*:Update")
	beego.Router("/type/view/:id", &home.TypeController{}, "*:View")
	beego.Router("/type/delete/:id", &home.TypeController{}, "Get:Delete")
	beego.AutoRouter(&home.StuffController{})
	beego.Router("/stuff/update/:id", &home.StuffController{}, "*:Update")
	beego.Router("/stuff/view/:id", &home.StuffController{}, "*:View")
	beego.Router("/stuff/delete/:id", &home.StuffController{}, "Get:Delete")
	beego.AutoRouter(&home.ToolController{})

	beego.Run()
}
