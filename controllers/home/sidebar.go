package home

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/config/yaml"
	// "github.com/astaxie/beego/config"
	"path/filepath"
	"store/controllers"
)

type SidebarController struct {
	controllers.BaseController
}

type cnfRow struct {
	Name  string      `json:"name"`
	Items interface{} `json:"items"`
}

func (this *SidebarController) Get() {
	this.TplNames = "default/login.html"

	var cnfPath string = filepath.Dir(beego.AppConfigPath) + "/sidebar.yaml"
	cnf, err := yaml.ReadYmlReader(cnfPath)
	if err != nil {
		fmt.Println(err)
	}

	var cnfRows = []*cnfRow{}

	for k, item := range cnf {
		//beego.Info(reflect.TypeOf(item))
		cnfRows = append(cnfRows, &cnfRow{k, item})
	}

	//fmt.Printf("%#v", cnf)
	this.Data["json"] = cnfRows
	this.ServeJson()
}
