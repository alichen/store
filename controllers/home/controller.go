package home

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"log"
	"store/controllers/components/datagrid"
	"store/models"
)

type ContainerController struct {
	datagrid.ManagerController
}

func (this *ContainerController) Prepare() {
	this.BaseController.Prepare()
	this.ManagerController.Dper = &models.Container{}
	this.DB = "default"
	this.Config = map[string]string{
		"indexTpl": "container/index.html",
		"formTpl":  "container/form.html",
		//"viewTpl":   "mall/goodspec/view.html",
		//"searchTpl": "container/search.html",
		//"batchTpl":  "container/batch.html",
		"viewUrl": "/container/view/",
	}
}

func (this *ContainerController) Combo() {
	o := orm.NewOrm()
	o.Using("default")
	var containers []orm.Params
	qs := o.QueryTable(new(models.Container))
	_, err := qs.OrderBy("Name").Values(&containers, "Id", "Name")
	if err != nil {
		log.Fatal(err)
	}
	this.Data["json"] = containers
	this.ServeJson()
}

type TypeController struct {
	datagrid.ManagerController
}

func (this *TypeController) Prepare() {
	this.BaseController.Prepare()
	this.ManagerController.Dper = &models.Type{}
	this.DB = "default"
	this.Config = map[string]string{
		"indexTpl": "type/index.html",
		"formTpl":  "type/form.html",
		//"viewTpl":   "mall/goodspec/view.html",
		//"searchTpl": "type/search.html",
		//"batchTpl":  "type/batch.html",
		"viewUrl": "/type/view/",
	}
}

func (this *TypeController) Combo() {
	o := orm.NewOrm()
	o.Using("default")
	var types []orm.Params
	qs := o.QueryTable(new(models.Type))
	_, err := qs.OrderBy("Name").Values(&types, "Id", "Name")
	if err != nil {
		log.Fatal(err)
	}
	this.Data["json"] = types
	this.ServeJson()
}

type StuffController struct {
	datagrid.ManagerController
}

func (this *StuffController) Prepare() {
	this.BaseController.Prepare()
	this.ManagerController.Dper = &models.Stuff{}
	this.DB = "default"
	this.Config = map[string]string{
		"indexTpl":  "stuff/index.html",
		"formTpl":   "stuff/form.html",
		"viewTpl":   "stuff/view.html",
		"searchTpl": "stuff/search.html",
		"batchTpl":  "stuff/batch.html",
		"viewUrl":   "/stuff/view/",
	}
}

func (this *StuffController) Create() {
	model := new(models.Stuff)
	if this.Ctx.Input.Is("POST") {
		if err := this.ParseForm(model); err != nil {
			log.Fatal(err)
		}
		typeId, err := this.GetInt64("TypeId")
		model.Type = &models.Type{Id: typeId}
		ContainerId, err := this.GetInt64("ContainerId")
		model.Container = &models.Container{Id: ContainerId}
		o := orm.NewOrm()
		qs := o.QueryTable(model)
		in, _ := qs.PrepareInsert()
		Id, err := in.Insert(model)
		if err != nil {
			log.Fatal(err)
		}
		this.Redirect(fmt.Sprintf("%s%d", this.Config["viewUrl"], Id), 302)
	}
	this.Data["model"] = model
	this.TplNames = this.Config["formTpl"]
}

func (this *StuffController) Update() {
	Id, err := this.GetInt(":id")
	if err != nil {
		this.Loger.Error("stuff Id:%v", err)
	}
	model := new(models.Stuff)
	o := orm.NewOrm()
	o.Using("mall")
	qs := o.QueryTable(model)
	qs = qs.Filter("Id__exact", Id)
	err = qs.One(model)
	if err != nil {
		this.Loger.Error("stuff cant find:%v", err)
	}
	if this.Ctx.Input.Is("POST") {
		if err := this.ParseForm(model); err != nil {
			log.Fatal(err)
		}
		typeId, err := this.GetInt64("TypeId")
		model.Type = &models.Type{Id: typeId}
		ContainerId, err := this.GetInt64("ContainerId")
		model.Container = &models.Container{Id: ContainerId}
		_, err = o.Update(model)
		if err != nil {
			log.Fatal(err)
		}
		this.Redirect(fmt.Sprintf("%s%d", this.Config["viewUrl"], Id), 302)
	}

	this.Data["model"] = model
	this.TplNames = this.Config["formTpl"]
}
