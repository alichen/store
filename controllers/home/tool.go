package home

import (
	"fmt"
	"github.com/astaxie/beego"
	//"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"store/controllers"
	"store/models"
	"strings"
)

type ToolController struct {
	controllers.BaseController
}

func (this *ToolController) MakeSn() {
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.Stuff))

	var stuffs []*models.Stuff
	_, err := qs.All(&stuffs)
	if err != nil {
		log.Fatal(err)
	}
	for _, stuff := range stuffs {
		name := stuff.Name
		rx := regexp.MustCompile(`_\w+$`)
		result := rx.Find([]byte(name))
		sn := strings.Replace(string(result), "_", "", -1)
		stuff.Sn = sn
		//path := strings.Replace(stuff.Path, `F:\`, ``, -1)
		//stuff.Path = path
		id, err := o.Update(stuff)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("the %d stuff sn updated to %s", id, sn)
	}
	this.Ctx.WriteString("makesn")
}

func (this *ToolController) Walk() {
	disk := beego.AppConfig.String("disk")
	relatePath := beego.AppConfig.String("path")
	path := disk + relatePath
	list := []string{}
	stuffs := []*models.Stuff{}
	filepath.Walk(path, func(destPath string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			infoStr := fmt.Sprintf("%s-%s:%d", destPath, info.Name(), info.Size())
			list = append(list, infoStr)

			modTime := info.ModTime().Unix()
			//fmt.Println(destPath)
			//_destPath := strings.Replace(destPath, `F:\`, "", -1)
			//fmt.Println(_destPath)
			fileName := strings.Split(info.Name(), ".")
			ext := fileName[1]
			if ext == "avi" || ext == "wmv" || ext == "mkv" || ext == "rmvb" {
				stuff := &models.Stuff{
					Name:       fileName[0],
					Ext:        ext,
					Path:       relatePath,
					ModifyTime: modTime,
				}
				//fmt.Printf("%v\n\r", stuff)
				stuffs = append(stuffs, stuff)
			}
		}
		return nil
	})
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.Stuff))
	i, _ := qs.PrepareInsert()
	for _, stuff := range stuffs {
		id, err := i.Insert(stuff)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(id, "insert!")
	}
	i.Close()
	//fmt.Printf("%v", stuffs)
	this.Ctx.WriteString(strings.Join(list, "\n\r"))
}
