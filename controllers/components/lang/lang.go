package lang

/*
*i18n翻译
 */
func T(src interface{}) (dst string) {
	var lang map[string]string = EnToCN

	_src, ok := src.(string)
	if !ok {
		return ""
	}
	if langStr, ok := lang[_src]; !ok {
		dst = _src
	} else {
		dst = langStr
	}
	return dst
}
