package helper

import (
	"fmt"
	"html/template"
	"reflect"
	"store/controllers/components/lang"
	"strings"
)

/*查看页面字段排序*/
func ViewFields(model interface{}, fields []string) (content template.HTML) {
	objT := reflect.TypeOf(model)
	objV := reflect.ValueOf(model)
	if !(objT.Kind() == reflect.Ptr && objT.Elem().Kind() == reflect.Struct) {
		return template.HTML("")
	}
	objT = objT.Elem()
	objV = objV.Elem()

	var raw []string
	for _, field := range fields {
		fieldT, found := objT.FieldByName(field)
		if !found {
			continue
		}
		fieldV := objV.FieldByName(field)
		tags := strings.Split(fieldT.Tag.Get("form"), ",")
		var tag string
		if len(tags) < 3 || len(tags[2]) == 0 {
			tag = fieldT.Name
		} else {
			tag = tags[2]
		}
		raw = append(raw, fmt.Sprintf("<tr><th>%v</th><td>%v</td></tr>", lang.T(tag), fieldV.Interface()))
	}
	return template.HTML(strings.Join(raw, ""))
}

/*导出页面字段排序*/
func ExportFields(model interface{}, fields []string) (content template.HTML) {
	objT := reflect.TypeOf(model)
	if !(objT.Kind() == reflect.Ptr && objT.Elem().Kind() == reflect.Struct) {
		return template.HTML("")
	}
	objT = objT.Elem()

	var raw []string
	//fmt.Printf("%v", fields)
	for _, field := range fields {
		fieldName := field
		_field := strings.Split(field, "__")
		if len(_field) > 1 {
			field = _field[0]
		}
		fieldT, found := objT.FieldByName(field)
		if !found {
			continue
		}
		tags := strings.Split(fieldT.Tag.Get("form"), ",")
		var tag string
		if len(tags) < 3 || len(tags[2]) == 0 {
			tag = fieldT.Name
		} else {
			tag = tags[2]
		}
		raw = append(raw, fmt.Sprintf(`<div class="pure-u-1">
			<div class="form-widget">
			<span class="form-label"><label>%s</label></span>
			<span class="form-input"><input name="%s" type="checkbox" value="" class="export_check check_row"/></span>
			</div></div>`, lang.T(tag), fieldName))
	}
	return template.HTML(strings.Join(raw, ""))
}

/*格式化form字段值*/
func FormateValue(value interface{}) (fmtValue string) {
	vT := reflect.TypeOf(value)
	vV := reflect.ValueOf(value)

	switch vT.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		fmtValue = fmt.Sprintf("%d", vV.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		fmtValue = fmt.Sprintf("%d", vV.Uint())
	case reflect.Float32, reflect.Float64:
		fmtValue = fmt.Sprintf("%.2f", vV.Float())
	case reflect.Interface:
		fmtValue = fmt.Sprintf("%v", vV.Interface())
	case reflect.String:
		fmtValue = strings.Trim(vV.String(), " ")
	}
	if fmtValue == "0" || fmtValue == "0.00" {
		fmtValue = ""
	}
	return fmtValue
}

/*扩展原有renderForm(在不需要yaui组件的情况下)*/
var unKind = map[reflect.Kind]bool{
	reflect.Uintptr:       true,
	reflect.Complex64:     true,
	reflect.Complex128:    true,
	reflect.Array:         true,
	reflect.Chan:          true,
	reflect.Func:          true,
	reflect.Map:           true,
	reflect.Ptr:           true,
	reflect.Slice:         true,
	reflect.Struct:        true,
	reflect.UnsafePointer: true,
}

func SimpleForm(obj interface{}, fields []string) template.HTML {
	objT := reflect.TypeOf(obj)
	objV := reflect.ValueOf(obj)
	if !(objT.Kind() == reflect.Ptr && objT.Elem().Kind() == reflect.Struct) {
		return template.HTML("")
	}
	objT = objT.Elem()
	objV = objV.Elem()

	var raw []string
	for i := 0; i < objT.NumField(); i++ {
		fieldV := objV.Field(i)
		if !fieldV.CanSet() || unKind[fieldV.Kind()] {
			continue
		}

		fieldT := objT.Field(i)
		found := false

		for _, field := range fields {
			if field == fieldT.Name {
				found = true
			}
		}
		if !found {
			continue
		}

		tags := strings.Split(fieldT.Tag.Get("form"), ",")
		label := fieldT.Name + ": "
		name := fieldT.Name
		fType := "text"

		switch len(tags) {
		case 1:
			if tags[0] == "-" {
				continue
			}
			if len(tags[0]) > 0 {
				name = tags[0]
			}
		case 2:
			if len(tags[0]) > 0 {
				name = tags[0]
			}
			if len(tags[1]) > 0 {
				fType = tags[1]
			}
		case 3:
			if len(tags[0]) > 0 {
				name = tags[0]
			}
			if len(tags[1]) > 0 {
				fType = tags[1]
			}
			if len(tags[2]) > 0 {
				label = tags[2]
			}
		}

		var value string
		switch fieldT.Type.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			value = fmt.Sprintf("%d", fieldV.Int())
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			value = fmt.Sprintf("%d", fieldV.Uint())
		case reflect.Float32, reflect.Float64:
			value = fmt.Sprintf("%.2f", fieldV.Float())
		case reflect.Interface:
			value = fmt.Sprintf("%v", fieldV.Interface())
		}
		if value == "0" || value == "0.00" {
			value = ""
		}

		//修复默认值为0的情况
		ormTag := fieldT.Tag.Get("orm")
		if ok := strings.Contains(ormTag, "default(0)"); ok {
			value = "0"
		}

		raw = append(raw, fmt.Sprintf(`<div class="pure-u-1"><div class="form-widget"><span class="form-label">
			<label class="required" for="%v">%v<span class="required">*</span></label></span><span class="form-input">
			<input name="%v" type="%v" value="%v"></span>
			</div></div>`,
			name, label, name, fType, value))
	}
	return template.HTML(strings.Join(raw, ""))
}
