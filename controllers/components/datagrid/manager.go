package datagrid

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type ManagerController struct {
	DataGridController
}

func (m *ManagerController) Index() {
	m.TplNames = m.Config["indexTpl"]
}

func (m *ManagerController) Create() {
	model := m.Dper
	if m.Ctx.Input.Is("POST") {
		if err := m.ParseForm(model); err != nil {
			log.Fatal(err)
		}
		o := orm.NewOrm()
		o.Using(m.DB)
		qs := o.QueryTable(m.Dper)
		in, _ := qs.PrepareInsert()
		Id, err := in.Insert(model)
		if err != nil {
			log.Fatal(err)
		}
		m.Redirect(fmt.Sprintf("%s%d", m.Config["viewUrl"], Id), 302)
	}
	m.Data["model"] = model
	m.TplNames = m.Config["formTpl"]
}

func (m *ManagerController) Update() {
	Id, err := m.GetInt(":id")
	if err != nil {
		log.Fatal(err)
	}

	o := orm.NewOrm()
	o.Using(m.DB)
	qs := o.QueryTable(m.Dper)
	qs = qs.Filter("Id__exact", Id)

	model := m.Dper
	err = qs.RelatedSel().One(model)

	if err != nil {
		log.Fatal(err)
	}

	if m.Ctx.Input.Is("POST") {
		if err := m.ParseForm(model); err != nil {
			log.Fatal(err)
		}

		_, err = o.Update(model)
		if err != nil {
			log.Fatal(err)
		}
		//	Id, _ := m.Id()
		m.Redirect(fmt.Sprintf("%s%d", m.Config["viewUrl"], Id), 302)
	}

	m.Data["model"] = model
	m.TplNames = m.Config["formTpl"]
}

func (m *ManagerController) Delete() {
	defer func() {
		if err := recover(); err != nil {
			m.Data["json"] = map[string]interface{}{
				"status": false,
				"info":   err,
			}
			m.ServeJson()
		}
	}()

	//param := m.Ctx.Input.Param
	Id := m.GetString(":id")
	/*	if err != nil {
		m.Loger.Error("%v", err)
	}*/
	//Id := param["1"]
	IdArr := strings.Split(Id, ",")
	_IdArr := []interface{}{}
	if len(IdArr) > 0 {
		for _, id := range IdArr {
			_IdArr = append(_IdArr, id)
		}
	}

	o := orm.NewOrm()
	o.Using(m.DB)
	qs := o.QueryTable(m.Dper)
	//qs, _ := m.GetId()

	num, _ := qs.Filter("Id", _IdArr...).Delete()

	resJson := make(map[string]interface{})

	resJson["status"] = true
	resJson["info"] = num

	m.Data["json"] = resJson
	m.ServeJson()
}

func (m *ManagerController) View() {
	Id, err := m.GetInt(":id")
	if err != nil {
		log.Fatal(err)
	}
	o := orm.NewOrm()
	o.Using(m.DB)
	qs := o.QueryTable(m.Dper)

	model := m.Dper
	fields := m.Dper.Fields("view")
	err = qs.Filter("Id", Id).RelatedSel().One(model, fields...)

	if err != nil {
		log.Fatal(err)
	}

	m.Data["model"] = model
	m.Data["fields"] = fields
	if _, ok := m.Config["viewTpl"]; !ok {
		m.Config["viewTpl"] = "datagrid/gridview.html"
	}
	m.Layout = "layout/window.html"
	m.TplNames = m.Config["viewTpl"]
}

type uploadResp struct {
	Name string `json:"name"`
	//size         int
	Url   string `json:"url"`
	State string `json:"state"`
	//	ThumbnailUrl string `json:"thumbnailUrl"`
	//	DeleteUrl    string `json:"deleteUrl"`
	//	DeleteType   string `json:"deleteType"`
}

type uploadRespError struct {
	Name string `json:"name"`
	//size int
	Err string `json:"error"`
}

func (m *ManagerController) Upload() {
	file, header, _ := m.GetFile("fileData")
	defer file.Close()

	originFileName := header.Filename
	ext := strings.Split(originFileName, ".")[1]
	//r := rand.New(rand.NewSource(time.Now().UnixNano()))
	toFileBase := beego.GetRandomString(10)
	toFileName := fmt.Sprintf("%s.%s", toFileBase, ext)

	rootPath := "/static/upload"
	year, month, day := time.Now().Date()
	subPath := fmt.Sprintf("/%d/%d/%d", year, month, day)
	destPath := rootPath + subPath
	absDestPath, _ := filepath.Abs(destPath)
	if _, err := os.Stat(absDestPath); os.IsNotExist(err) {
		os.MkdirAll(absDestPath, 0666)
	}

	toFile := absDestPath + "/" + toFileName
	f, _ := os.OpenFile(toFile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	defer f.Close()
	io.Copy(f, file)

	defer func() {
		if err := recover(); err != nil {
			m.Data["json"] = map[string][]uploadRespError{
				"files": []uploadRespError{
					uploadRespError{
						Name: originFileName,
						Err:  fmt.Sprintf("%v", err),
					},
				},
			}
			m.ServeJson()
		}
	}()

	resJson := map[string][]uploadResp{
		"files": []uploadResp{
			uploadResp{
				Name:  toFileName,
				Url:   destPath,
				State: "SUCCESS",
				//	ThumbnailUrl: toFile,
				//	DeleteUrl:    toFile,
				//	DeleteType:   "DELETE",
			},
		},
	}
	m.Data["json"] = resJson
	m.ServeJson()
}

func (m *ManagerController) Attach() {
	file, header, _ := m.GetFile("upfile")
	defer file.Close()

	originFileName := header.Filename
	ext := strings.Split(originFileName, ".")[1]
	//r := rand.New(rand.NewSource(time.Now().UnixNano()))
	toFileBase := beego.GetRandomString(10)
	toFileName := fmt.Sprintf("%s.%s", toFileBase, ext)

	rootPath := "/static/attach"
	year, month, day := time.Now().Date()
	subPath := fmt.Sprintf("/%d/%d/%d", year, month, day)
	destPath := rootPath + subPath
	absDestPath, _ := filepath.Abs(destPath)
	if _, err := os.Stat(absDestPath); os.IsNotExist(err) {
		os.MkdirAll(absDestPath, 0666)
	}

	toFile := absDestPath + "/" + toFileName
	f, _ := os.OpenFile(toFile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	defer f.Close()
	io.Copy(f, file)

	defer func() {
		if err := recover(); err != nil {
			m.Data["json"] = map[string][]uploadRespError{
				"files": []uploadRespError{
					uploadRespError{
						Name: originFileName,
						Err:  fmt.Sprintf("%v", err),
					},
				},
			}
			m.ServeJson()
		}
	}()

	destFile := destPath + "/" + toFileName
	if reqType := m.GetString("type"); reqType == "ajax" {
		m.Ctx.WriteString(destFile)
	}
	editorId := m.GetString("editorid")
	m.Ctx.WriteString("<script>parent.UM.getEditor('" + editorId + "').getWidgetCallback('image')('" + destFile + "','SUCCESS')</script>")
	/*	resJson := map[string][]uploadResp{
			"files": []uploadResp{
				uploadResp{
					Name:  toFileName,
					Url:   destPath,
					State: "SUCCESS",
					//	ThumbnailUrl: toFile,
					//	DeleteUrl:    toFile,
					//	DeleteType:   "DELETE",
				},
			},
		}
		m.Data["json"] = resJson
		m.ServeJson()*/
}
