package datagrid

import (
	"github.com/astaxie/beego/orm"
)

type DataProvider interface {
	Init(interface{}, []string)
	Fields(string) []string
	SetCriteria(map[string]interface{}) error
	GetNum() (int64, error)
	GetData() (int64, []orm.Params, error)
	FormateData([]orm.Params) []orm.Params
	ExportData([]string) (int64, []orm.ParamsList, error)
	BatchData(map[string]interface{}) (int64, error)
}
