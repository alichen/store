package datagrid

import (
	"encoding/csv"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"log"
	//"mymall/controllers/components"
	"os"
	"path/filepath"
	"time"
)

var dataProvider DataProvider

func SetDataProvider(dper DataProvider) {
	dataProvider = dper
}

func GetDataProvider() DataProvider {
	return dataProvider
}

func ExportToFile(headers []string, objs []orm.ParamsList, path string) string {
	subPath := beego.Date(time.Now(), "Y-m-d")
	exportPath := path + subPath
	absExportPath, _ := filepath.Abs(exportPath)
	if _, err := os.Stat(absExportPath); os.IsNotExist(err) {
		os.MkdirAll(absExportPath, 0666)
	}

	toFileName := beego.GetRandomString(10)
	toFile := absExportPath + "/" + toFileName + ".csv"
	downToken := subPath + "_" + toFileName + ".csv"
	f, _ := os.OpenFile(toFile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	defer f.Close()
	csvW := csv.NewWriter(f)

	for j := 0; j < len(headers); j++ {
		//headers[j] = controllers.T(headers[j])
	}
	csvW.Write(headers) //写入header
	for _, obj := range objs {
		record := make([]string, 0, len(obj))
		for i := 0; i < len(obj); i++ {
			record = append(record, fmt.Sprintf("%v", obj[i]))
		}
		csvW.Write(record)
	}
	csvW.Flush()
	if err := csvW.Error(); err != nil {
		log.Fatal(err)
	}
	return downToken
}
