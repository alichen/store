package datagrid

import (
	"github.com/astaxie/beego/orm"
	"log"
	"path/filepath"
	"store/controllers"
	"store/controllers/components/lang"
	"strconv"
	"strings"
)

type DataGridController struct {
	controllers.BaseController
	DB     string
	Dper   DataProvider
	Config map[string]string
}

func (m *DataGridController) GridData(dp DataProvider) {
	criteria := make(map[string]interface{})

	defer func() {
		if err := recover(); err != nil {
			log.Fatal(err)
			m.Data["json"] = map[string]interface{}{
				"total": 0,
				"rows":  []interface{}{},
				"err":   err,
			}
			m.ServeJson()
		}
	}()

	criteria["model"] = dp
	criteria["form"] = m.Input()
	var sortOrder []string
	if sortBy := m.GetString("sortBy"); sortBy != "" {
		sortDir := m.GetString("sortDir")
		if sortDir == "desc" {
			sortOrder = append(sortOrder, "-"+sortBy)
		} else {
			sortOrder = append(sortOrder, sortBy)
		}
	}
	criteria["orderby"] = sortOrder

	o := orm.NewOrm()
	o.Using(m.DB)
	qs := o.QueryTable(dp)
	dp.Init(qs, dp.Fields("list")) //注入querysetter
	dp.SetCriteria(criteria)
	SetDataProvider(dp)
	num, _ := dp.GetNum()

	//分页开始
	if start := m.GetString("start"); start != "" {
		criteria["start"], _ = strconv.Atoi(start)
	}
	if limit := m.GetString("limit"); limit != "" {
		criteria["limit"], _ = strconv.Atoi(limit)
	}

	dp.SetCriteria(criteria)
	_, res, _ := dp.GetData()
	res = dp.FormateData(res)

	resJson := struct {
		Total int64        `json:"total"`
		Items []orm.Params `json:"rows"`
	}{
		Total: num,
		Items: res,
	}

	m.Data["json"] = resJson
	m.ServeJson()
}

func (m *DataGridController) Load() {
	if m.Ctx.Input.Is("POST") {
		m.GridData(m.Dper)
	}
	m.Data["form"] = m.Dper
	m.Data["fields"] = m.Dper.Fields("search")
	if _, ok := m.Config["searchTpl"]; !ok {
		m.Config["searchTpl"] = "datagrid/gridsearch.html"
	}
	m.Layout = "layout/window.html"
	m.TplNames = m.Config["searchTpl"]
}

func (m *DataGridController) BatchData(dp DataProvider) {
	data := map[string]interface{}{
		"form":  m.Input(),
		"model": dp,
	}
	o := orm.NewOrm()
	o.Using(m.DB)
	qs := o.QueryTable(m.Dper)
	dp.Init(qs, dp.Fields("batch")) //fields could be null
	num, _ := dp.BatchData(data)

	defer func() {
		if err := recover(); err != nil {
			m.Data["json"] = map[string]interface{}{
				"status": false,
				"info":   lang.T(err),
			}
			m.ServeJson()
		}
	}()

	resJson := make(map[string]interface{})

	resJson["status"] = true
	resJson["info"] = num

	m.Data["json"] = resJson
	m.ServeJson()
}

func (m *DataGridController) Batch() {
	if m.Ctx.Input.Is("POST") {
		m.BatchData(m.Dper)
	}
	//m.GridSearch(&models.Goods{})
	m.Data["form"] = m.Dper
	m.Data["fields"] = m.Dper.Fields("batch")
	if _, ok := m.Config["batchTpl"]; !ok {
		m.Config["batchTpl"] = "datagrid/gridbatch.html"
	}
	m.Layout = "layout/window.html"
	m.TplNames = m.Config["batchTpl"]
}

func (m *DataGridController) Export() {
	model := GetDataProvider() //need query conditions
	if exportCols := m.GetString("columns"); exportCols != "" && m.IsAjax() {
		//dper := LoadDper
		criteria := make(map[string]interface{})
		ids := m.GetString("ids")
		if ids != "" {
			_ids := strings.Split(ids, ",")
			__ids := make([]interface{}, len(_ids))
			for _, _id := range _ids {
				__ids = append(__ids, _id)
			}
			criteria["ids"] = __ids
		}
		criteria["start"] = 0  //remove page condition
		criteria["limit"] = -1 //remove page condition
		model.SetCriteria(criteria)

		_exportCols := strings.Split(exportCols, ",")
		_, res, err := model.ExportData(_exportCols)
		if err != nil {
			log.Fatal(err)
		}
		downToken := ExportToFile(_exportCols, res, "/static/export/")
		m.Ctx.WriteString(downToken)
	}
	ids := m.GetString("ids")
	fields := model.Fields("export")
	m.Data["fields"] = fields
	m.Data["model"] = model
	m.Data["ids"] = ids
	//m.Data["model"] = model
	if _, ok := m.Config["exportTpl"]; !ok {
		m.Config["exportTpl"] = "datagrid/gridexport.html"
	}
	m.Layout = "layout/window.html"
	m.TplNames = m.Config["exportTpl"]
}

func (m *DataGridController) DownExport() {
	token := m.GetString(":file")
	tokenArr := strings.Split(token, "_")
	filePath := "/static/export/" + tokenArr[0] + "/" + tokenArr[1]
	absFilePath, _ := filepath.Abs(filePath)
	m.Ctx.Output.Download(absFilePath)
}
