package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"store/controllers/components/helper"
	"store/controllers/components/lang"
)

type BaseController struct {
	beego.Controller
	Loger *logs.BeeLogger
}

func (this *BaseController) Prepare() {
	this.Loger = setupLogger()
	this.Layout = "layout/main.html"
}

func init() {
	//beego.SetStaticPath("/static", "static")
	beego.AddFuncMap("t", lang.T)
	beego.AddFuncMap("simpleform", helper.SimpleForm)
	beego.AddFuncMap("fmtValue", helper.FormateValue)
	beego.AddFuncMap("viewFields", helper.ViewFields)
	beego.AddFuncMap("exportFields", helper.ExportFields)
}

func setupLogger() *logs.BeeLogger {
	log := logs.NewLogger(10000)
	log.SetLogger("console", `{"level":1}`) //devlopment
	//log.SetLogger("file", `{"filename":"logs/mymall.log"}`) deploy
	return log
}

/*debug*/
func (this *BaseController) TVarl(ins ...interface{}) {
	if len(ins) == 1 {
		fmt.Println(ins[0])
	} else {
		for k, in := range ins {
			ins[k] = fmt.Sprintf("v", in)
		}
		fmt.Println(ins...)
	}
}

func (this *BaseController) TType(in interface{}) {
	fmt.Printf("%T", in)
}
