package models

import (
	"fmt"
	"strconv"
	//"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"time"
)

type Container struct {
	DataGridModel `orm:"-"`
	Id            int64  `orm:"auto;pk" form:"-"`
	Name          string `orm:"size(20)" form:"Name"`
	Size          string `orm:"size(20)"`
	Memo          string `orm:"size(100)"`
}

func (m *Container) TableName() string {
	return "container"
}

func (m *Container) String() string {
	return fmt.Sprintf("%s", m.Name)
}

func (m *Container) Fields(render string) []string {
	columns := make([]string, 0, 6)
	switch render {
	case "list":
		columns = []string{
			"Id", "Name", "Size", "Memo",
		}
	case "search":
		columns = []string{
			"Name", "Size", "Memo",
		}
	case "batch":
		columns = []string{
			"Name", "Size", "Memo",
		}
	case "export":
		columns = []string{
			"Id", "Name", "Size", "Memo",
		}
	case "view":
		columns = []string{
			"Id", "Name", "Size", "Memo",
		}
	}
	return columns
}

type Type struct {
	DataGridModel `orm:"-"`
	Id            int64  `orm:"auto;pk" form:"-"`
	Name          string `orm:"size(20)" form:"Name"`
	Memo          string `orm:"size(100)"`
}

func (m *Type) TableName() string {
	return "type"
}

func (m *Type) String() string {
	return fmt.Sprintf("%s", m.Name)
}

func (m *Type) Fields(render string) []string {
	columns := make([]string, 0, 6)
	switch render {
	case "list":
		columns = []string{
			"Id", "Name", "Memo",
		}
	case "search":
		columns = []string{
			"Name", "Memo",
		}
	case "batch":
		columns = []string{
			"Name", "Memo",
		}
	case "export":
		columns = []string{
			"Id", "Name", "Memo",
		}
	case "view":
		columns = []string{
			"Id", "Name", "Memo",
		}
	}
	return columns
}

type Stuff struct {
	DataGridModel `orm:"-"`
	Id            int64      `orm:"auto;pk" form:"-"`
	Container     *Container `orm:"rel(fk);null" form:"ContainerId,,Container"`
	Type          *Type      `orm:"rel(fk);null" form:"TypeId,,Type"`
	Name          string     `orm:"size(250)" form:"Name"`
	Sn            string     `orm:"size(20);null"`
	Actor         string     `orm:"size(50);null"`
	Cover         string     `orm:"size(250);null"`
	Path          string     `orm:"size(200)"`
	Ext           string     `orm:"size(20)"`
	ModifyTime    int64      `orm:"default(0)" form:"ModifyTime"`
}

func (m *Stuff) TableName() string {
	return "stuff"
}

func (m *Stuff) Fields(render string) []string {
	columns := make([]string, 0, 6)
	switch render {
	case "list":
		columns = []string{
			"Id", "Type__Name", "Container__Name", "Name", "Sn", "Actor", "Path", "Ext", "ModifyTime",
		}
	case "search":
		columns = []string{
			"Name", "Type", "Container", "Actor", "Path", "Sn", "Ext",
		}
	case "batch":
		columns = []string{
			"Name", "Type", "Container", "Actor", "Path", "Sn", "Ext",
		}
	case "export":
		columns = []string{
			"Id", "Type__Name", "Container__Name", "Name", "Sn", "Actor", "Cover", "Path", "Ext", "ModifyTime",
		}
	case "view":
		columns = []string{
			"Id", "Type", "Container", "Name", "Sn", "Path", "Ext", "ModifyTime",
		}
	}
	return columns
}

func (m *Stuff) FormateData(res []orm.Params) []orm.Params {
	for k, row := range res {
		modifyTime, _ := strconv.ParseInt(fmt.Sprintf("%d", row["ModifyTime"]), 10, 64)
		row["ModifyTime"] = time.Unix(modifyTime, 0).Format("2006-01-02 15:04:05")
		res[k] = row
	}
	return res
}
