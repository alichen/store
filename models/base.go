package models

import (
	"github.com/astaxie/beego/orm"
	//_ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
)

func init() {
	orm.RegisterDriver(`sqlite3`, orm.DR_Sqlite)
	//orm.RegisterDriver(`mysql`, orm.DR_MySQL)
	orm.Debug = true
	//maxIdle := 30
	//maxConn := 30
	orm.RegisterDataBase(`default`, `sqlite3`, "./data/store.db")
	//orm.RegisterDataBase(`default`, `mysql`, "root:123456@tcp(127.0.0.1:3306)/store?charset=utf8")

	orm.RegisterModel(new(Container))
	orm.RegisterModel(new(Type))
	orm.RegisterModel(new(Stuff))
}
