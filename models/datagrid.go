package models

import (
	"errors"
	"fmt"
	//"fmt"
	"github.com/astaxie/beego/orm"
	"net/url"
	"reflect"
	"strconv"
	"strings"
)

type DataGridModel struct {
	qs     orm.QuerySeter `orm:"-"`
	fields []string
}

func paramsGenerator(form url.Values, obj interface{}) (orm.Params, error) {
	params := orm.Params{}
	objT := reflect.TypeOf(obj)
	objT = objT.Elem()

	for i := 0; i < objT.NumField(); i++ {
		fieldT := objT.Field(i)
		tags := strings.Split(fieldT.Tag.Get("form"), ",")
		var tag string
		if len(tags) == 0 || len(tags[0]) == 0 {
			tag = fieldT.Name
		} else if tags[0] == "-" {
			continue
		} else {
			tag = tags[0]
		}

		value := form.Get(tag)
		if len(value) == 0 {
			continue
		}

		switch fieldT.Type.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			x, err := strconv.ParseInt(value, 10, 64)
			if err != nil {
				return nil, err
			}
			params[fieldT.Name] = x
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			x, err := strconv.ParseUint(value, 10, 64)
			if err != nil {
				return nil, err
			}
			params[fieldT.Name] = x
		case reflect.Float32, reflect.Float64:
			x, err := strconv.ParseFloat(value, 64)
			if err != nil {
				return nil, err
			}
			params[fieldT.Name] = x
		case reflect.String, reflect.Ptr, reflect.Interface:
			params[fieldT.Name] = value
		}
	}

	return params, nil
}

func (m *DataGridModel) BatchData(data map[string]interface{}) (num int64, err error) {
	qs := m.GetQuerySeter()

	form := data["form"].(url.Values)
	model := data["model"]
	ids := strings.Split(form.Get("batch_id"), ",")

	if len(ids) == 0 {
		err = errors.New("no rows selected")
		return 0, err
	}

	Ids := make([]interface{}, len(ids))
	for _, id := range ids {
		Ids = append(Ids, id)
	}

	params, err := paramsGenerator(form, model)
	if err != nil {
		return 0, err
	}
	num, err = qs.Filter("Id__in", Ids...).Update(params)
	fmt.Printf("%v", params)
	if err != nil {
		return 0, err
	}
	return num, nil
}

func (m *DataGridModel) Init(obj interface{}, fields []string) {
	if qs, ok := obj.(orm.QuerySeter); ok {
		m.qs = qs
	}
	m.fields = fields
}

func (m *DataGridModel) GetData() (num int64, res []orm.Params, err error) {
	num, err = m.qs.Values(&res, m.fields...)
	if err != nil {
		return 0, nil, err
	}
	return num, res, nil
}

func (m *DataGridModel) FormateData(data []orm.Params) []orm.Params {
	return data
}

func (m *DataGridModel) ExportData(fields []string) (num int64, res []orm.ParamsList, err error) {
	num, err = m.qs.ValuesList(&res, fields...)
	if err != nil {
		return 0, nil, err
	}
	return num, res, nil
}

func (m *DataGridModel) GetNum() (num int64, err error) {
	num, err = m.qs.Count()
	if err != nil {
		return 0, err
	}
	return num, nil
}

func (m *DataGridModel) GetQuerySeter() orm.QuerySeter {
	return m.qs
}

func condGenerator(form url.Values, obj interface{}) (cond *orm.Condition, err error) {
	cond = orm.NewCondition()
	objV := reflect.ValueOf(obj)
	objT := reflect.TypeOf(obj)
	objV = objV.Elem()
	objT = objT.Elem()

	for i := 0; i < objT.NumField(); i++ {
		//	fieldV := objV.Field(i)
		fieldT := objT.Field(i)
		tags := strings.Split(fieldT.Tag.Get("form"), ",")
		var tag string
		if len(tags) == 0 || len(tags[0]) == 0 {
			tag = fieldT.Name
		} else if tags[0] == "-" {
			continue
		} else {
			tag = tags[0]
		}

		value := form.Get(tag)
		if len(value) == 0 {
			continue
		}

		var condExpr string
		switch fieldT.Type.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			if strings.Contains(value, ",") {
				rangeArr := strings.Split(value, ",")

				if rangeArr[0] != "" {
					sub, err := strconv.ParseInt(rangeArr[0], 10, 64)
					if err != nil {
						return nil, err
					}
					condExpr = fieldT.Name + "__gte"
					cond = cond.And(condExpr, sub)
				}
				if rangeArr[1] != "" {
					sup, err := strconv.ParseInt(rangeArr[1], 10, 64)
					if err != nil {
						return nil, err
					}
					condExpr = fieldT.Name + "__lt"
					cond = cond.And(condExpr, sup)
				}
			} else {
				x, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					return nil, err
				}
				condExpr = fieldT.Name + "__exact"
				cond = cond.And(condExpr, x)
			}
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			if strings.Contains(value, ",") {
				rangeArr := strings.Split(value, ",")

				if rangeArr[0] != "" {
					sub, err := strconv.ParseUint(rangeArr[0], 10, 64)
					if err != nil {
						return nil, err
					}
					condExpr = fieldT.Name + "__gte"
					cond = cond.And(condExpr, sub)
				}
				if rangeArr[1] != "" {
					sup, err := strconv.ParseUint(rangeArr[1], 10, 64)
					if err != nil {
						return nil, err
					}
					condExpr = fieldT.Name + "__lt"
					cond = cond.And(condExpr, sup)
				}
			} else {
				x, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					return nil, err
				}
				condExpr = fieldT.Name + "__exact"
				cond = cond.And(condExpr, x)
			}
		case reflect.Float32, reflect.Float64:
			if strings.Contains(value, ",") {
				rangeArr := strings.Split(value, ",")

				if rangeArr[0] != "" {
					sub, err := strconv.ParseFloat(rangeArr[0], 64)
					if err != nil {
						return nil, err
					}
					condExpr = fieldT.Name + "__gte"
					cond = cond.And(condExpr, sub)
				}
				if rangeArr[1] != "" {
					sup, err := strconv.ParseFloat(rangeArr[1], 64)
					if err != nil {
						return nil, err
					}
					condExpr = fieldT.Name + "__lt"
					cond = cond.And(condExpr, sup)
				}
			} else {
				x, err := strconv.ParseFloat(value, 64)
				if err != nil {
					return nil, err
				}
				condExpr = fieldT.Name + "__exact"
				cond = cond.And(condExpr, x)
			}
		case reflect.String:
			condExpr = fieldT.Name + "__icontains"
			cond = cond.And(condExpr, value)
		case reflect.Ptr:
			condExpr = fieldT.Name + "__exact"
			cond = cond.And(condExpr, value)
		}
	}
	return cond, nil
}

func (m *DataGridModel) SetCriteria(criteria map[string]interface{}) error {
	qs := m.GetQuerySeter()
	if start, ok := criteria["start"]; ok {
		qs = qs.Offset(start)
	} else {
		qs = qs.Offset(0)
	}
	if limit, ok := criteria["limit"]; ok {
		qs = qs.Limit(limit)
	} else {
		qs = qs.Limit(10)
	}
	if orderBy, ok := criteria["orderby"]; ok {
		qs = qs.OrderBy(orderBy.([]string)...)
	}
	if form, ok := criteria["form"]; ok {
		if model, yes := criteria["model"]; yes {
			condition, err := condGenerator(form.(url.Values), model)
			if err != nil {
				return err
			}
			qs = qs.SetCond(condition)
		}
	}
	if ids, ok := criteria["ids"]; ok {
		qs = qs.Filter("Id__in", ids.([]interface{})...)
	}
	m.qs = qs
	return nil
}
