define('components/yaui-itemselector', function(require, exports, module) {
    var yauiItemSelector = {
        init: function(el) {
            var /*el=$('.yaui-itemselector',ctx),*/ self = this,
                config = el.attr("yaui-options"),
                valueEl = el.next(),
                value = valueEl.val(),
                options = {
                    "width": "400px",
                    "height": "302px",
                    onItemSelect: function() {
                        var selectedValues;
                        selectedValues = el.omItemSelector('value');
                        valueEl.val(selectedValues.join(','));
                    },
                    onItemDeselect: function() {
                        var selectedValues;
                        selectedValues = el.omItemSelector('value');
                        valueEl.val(selectedValues.join(','));
                    }
                };
            config = $.parseJSON(config);
            if (value !== '') {
                var value = value.split(',');
                for (var i = 0; i < value.length; i++) {
                    value[i] = parseInt(value[i])
                }
                config['value'] = value;
            }
            $.extend(options, config);
            el.removeAttr('yaui-options').removeClass("yaui-itemselector");
            el.omItemSelector(options);
        },
        build: function(ctx) {
            var self = this,
                el = $('.yaui-itemselector', ctx);
            el.each(function() {
                self.init($(this));
            });
        }
    };
    module.exports = yauiItemSelector;
});
