define('components/yaui-tree', function(require, exports, module) {
    var yauiTree = {
        init: function(el) {
            var $el = el,
                config = $el.attr("yaui-options"),
                valueEl = $el.next(),
                value = valueEl.val(),
                options = {
                    //cascadeCheck:false,
                    onSuccess: function(data) {
                        $el.omTree('expandAll'); //expand all nodes

                        var target = $el.omTree('findNodes', 'checked', true);
                        if (target) {
                            for (var i = 0; i < target.length; i++) {
                                $el.omTree('check', target[i]);
                            }
                        }
                    }
                };
            config = JSON.parse(config);
            $.extend(options, config);
            $el.removeAttr('yaui-options');
            $el.omTree(options);
        },
        build: function(ctx) {
            var self = this,
                el = $('.yaui-tree', ctx);
            el.each(function() {
                self.init($(this));
            });
        }
    }
    module.exports = yauiTree
});
