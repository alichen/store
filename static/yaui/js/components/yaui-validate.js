define('components/yaui-validate', function(require, exports, module) {
    var yauiValidate = {
        options: {
            //wrapper 显示错误信息的外层标签名称.
            //success:'' 指定校验成功没有任何错误后加到提示元素上面的样式名称
            validClass: 'valid' //指定校验成功没有任何错误后加到元素的class名称
            errorClass: 'error', //指定校验错误显示标签的class名称，此class也将添加在校验的元素上面。
            // errorPlacement 定制错误信息显示的回调方法。该方法有两个参数，第一个参数是错误信息的元素，第二个是触发校验错误的源元素
            focusCleanup: false, //获得焦点的时候是否清除错误提示，这种清除是针对所有元素的,如果设置为true，则必须将focusInvalid设置为false，否则将没有校验效果
            focusInvalid: true, //校验错误的时候是否将聚焦元素
            // groups 错误消息分组
            //ignore 校验时忽略指定的元素
            //invalidHandler 定制表单提交但校验不通过的回调方法
            //onclick 在checkbox和radio的click事件发生后是否进行校验
            //onfocusout 在blur事件发生时是否进行校验，如果没有输入任何值，则将忽略校验。
            //onkeyup 在keyup事件发生时是否进行校验。
            //onsubmit 否在提交时校验表单，如果设置为false，则提交的时候不校验表单，
            //validateOnEmpty 当所有的输入域为空时用tab或者鼠标切换输入域时是否进行校验
            showErrors: function(errorMap, errorList) {
                if (errorList && errorList.length > 0) { //如果存在错误信息
                    $.each(errorList, function(index, obj) { //遍历错误信息 ，index为错误信息的索引号，obj为当前错误信息对象
                        var msg = this.message; //获取当前错误信息文本
                        //这里编写一些代码处理你的错误信息
                    });
                } else {
                    //错误信息已经消除，此处要写隐藏错误信息的代码，
                    //通过this.currentElements获取当前对象
                }
                this.defaultShowErrors(); //如果还有默认的行为，请调用它。
            }
            //submitHandler 定制校验通过后表单提交前的回调方法
        },
        _parseRules: function() {
            var self = this,
                form = this.el,
                rules = {},
                messages = {},
                validInputs = form.find('.valid-row');
            $.each(validInputs, function() {
                var rule = $(this).attr('valid-rules'),
                    message = $(this).attr('valid-messages');
                rules[$(this).attr('name')] = $.parseJSON(rule);
                messages[$(this).attr('name')] = $.parseJSON(message);
            })
            this.rules = rules;
            this.messages = messages;
        },
        init: function(el) {
            var self = this,
                $el = el;
            this._parseRules();
            $.extend(this.options, {
                rules: this.rules,
                messages: this.messages
            });
            $el.removeAttr('yaui-validate');
            $el.validate(this.options);
        },
        build: function(ctx) {
            var self = this,
                el = $('.yaui-validate', ctx);
            el.each(function() {
                self.init($(this));
            });
        }
    };
    module.exports = yauiValidate;
});
