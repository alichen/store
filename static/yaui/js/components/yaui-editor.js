define('components/yaui-editor', ["vendor/ueditor/ueditor.config.js", "vendor/ueditor/ueditor.all.js"], function(require, exports, module) {
    window.UEDITOR_HOME_URL = '/static/yaui/js/vendor/ueditor/';
    var yauiEditor = {
        init: function(el) {
            var self = this,
                // el = $('.yaui-editor', ctx),
                config = el.attr("yaui-options"),
                options = {
                    initialFrameWidth: 810, //编辑器宽度
                    initialFrameHeight: 350, //编辑器高度
                    initialStyle: 'p{line-height:1em}', //编辑器层级的基数,可以用来改变字体等
                    elementPathEnabled: false, //是否启用元素路径，默认是显示
                    autoHeightEnabled: false, // 是否自动长高,默认true
                    toolbarTopOffset: 400, //编辑器底部距离工具栏高度(如果参数大于等于编辑器高度，则设置无效)
                };
            config = $.parseJSON(config);
            $.extend(options, config);
            el.removeAttr('yaui-options').removeClass("yaui-editor");
            require("vendor/ueditor/ueditor.config.js");
            require("vendor/ueditor/ueditor.all.js");
            // require.async(["vendor/ueditor/ueditor.config.js","vendor/ueditor/ueditor.all"],function(){
            UE.getEditor(el.attr('id'), options);
            // });
        },
        build: function(ctx) {
            var self = this,
                el = $('.yaui-editor', ctx);
            el.each(function() {
                self.init($(this));
            });
        }
    };
    module.exports = yauiEditor;
});
