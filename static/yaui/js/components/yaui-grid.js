define('components/yaui-grid', [], function(require, exports, module) {
    yaui = require('yaui');
    var yauiGrid = {
        init: function(el) {
            var self = this;
            this.el = el;
            this._initColumns();
            this._initToolbar();
            var buildOpts = {
                title: el.attr("title"),
                idField: el.attr("idField"),
                autoFit: true,
                limit: 19,
                showIndex: false,
                singleSelect: false,
                height: 'auto',
                method: 'POST',
                toolbars: self.toolbars,
                dataSource: el.attr("url"),
                colModel: self.colData
            };
            //dom清理
            el.empty().removeAttr('url').removeAttr('idField').removeAttr('title').removeClass('yaui-omgrid');
            el.omGrid(buildOpts);
        },
        build: function(ctx) {
            var self = this,
                el = $('.yaui-grid', ctx);
            el.each(function() {
                self.init($(this));
            });
        },
        _buildWindow: function(id, href, isAjax, data) {
            $('#' + id).empty();
            var _isAjax = isAjax || false;
            var _data = data || {};
            if (href) {
                if (_isAjax) {
                    $('#' + id).load(href + ' #container', function() {
                        yaui.init($('#' + id));
                    });
                } else {
                    $('#' + id).css('padding', 0);
                    $('#' + id).html('<iframe marginheight=0 marginwidth=0 width="100%" height="100%" frameborder="0" border="0" scrolling="auto"></iframe>');
                    $('#' + id).children().data('src', href);
                    //    $('<iframe marginheight=0 marginwidth=0 width="100%" height="100%" frameborder="0" border="0" scrolling="yes" src="'+href+'"></iframe>').appendTo($('#'+id));
                }
            }
            return $('#' + id);
        },
        _buildButton: function(text, iconCls) {
            var button = $("<div class='btn_wrap'><a href='#' class='" + iconCls + "'>" + text + "</a></div>");
            button.bind({
                'mouseover': function() {
                    $(this).addClass('btn_hover');
                },
                'mouseout': function() {
                    $(this).removeClass('btn_hover');
                }
            });
            //  return   button.omButton({width:80});
            return button;
        },
        _initToolbar: function() {
            var toolbars = {}, self = this.el,
                toolbarsEl = $("th", self);
            toolbarsEl.each(function() {
                //dialog default width and height
                var dialog = $("a", this).attr("dialog"),
                    dialogWH = dialog ? dialog.split(',') : [900, 500];
                toolbars[$("a", this).attr("name")] = {
                    "text": $("a", this).text(),
                    "href": $("a", this).attr("href"),
                    "width": dialogWH[0],
                    "height": dialogWH[1]
                }
            });
            this.toolbars = toolbars;

            this._injectToolbar();
        },
        /*注入omGrid组件的init调用*/
        _injectToolbar: function() {
            var buildWindow = this._buildWindow,
                buildButton = this._buildButton;
            //var injectFunc =

            $.omWidget.addInitListener('om.omGrid', function() {
                var self = this,
                    datasource = self.options.dataSource,
                    idField = self.options.idField,
                    toolbars = self.options.toolbars,
                    queryParams;
                if (!toolbars)
                    return;

                /*加入工具栏*/
                var toolbarDiv = $("<div class='toolbarDiv'></div>");
                var toolbarF = $('<div class="toolbarRowF"></div>').appendTo(toolbarDiv);
                if (toolbars['add']) {
                    var optText = toolbars['add']['text'],
                        optUrl = toolbars['add']['href'],
                        add_btn = buildButton(optText, 'icon-add').appendTo(toolbarF),
                        optWidth = toolbars['add']['width'],
                        optHeight = toolbars['add']['height'];
                    add_btn.after('<div class="datagrid-btn-separator" />');
                    add_btn.bind('click', {
                        text: optText,
                        url: optUrl,
                        width: optWidth,
                        height: optHeight
                    }, function(e) {
                        buildWindow('operate_window', e.data.url).omWindow({
                            title: e.data.text,
                            width: e.data.width,
                            height: e.data.height,
                            collapsible: true,
                            closeOnEscape: true,
                            modal: true,
                            onClose: function() {
                                self.reload();
                            }
                        });
                        return false;
                    });
                }
                if (toolbars['search']) {
                    var optText = toolbars['search']['text'],
                        optUrl = toolbars['search']['href'],
                        search_btn = buildButton(optText, 'icon-search').appendTo(toolbarF),
                        optWidth = toolbars['search']['width'],
                        optHeight = toolbars['search']['height'];
                    search_btn.after('<div class="datagrid-btn-separator" />');
                    search_btn.bind('click', {
                        text: optText,
                        url: optUrl,
                        width: optWidth,
                        height: optHeight
                    }, function(e) {
                        buildWindow('operate_window', e.data.url, true).omWindow({
                            title: e.data.text,
                            width: e.data.width,
                            height: e.data.height,
                            collapsible: false,
                            maximizable: false,
                            minimizable: false,
                            modal: true,
                            onClose: function() {
                                //self.reload();
                            }
                        });
                        return false;
                    });
                    /*处理搜索事件*/
                    $('.s_form_wrapper .check_row').live('click', function() {
                        if (this.checked)
                            $(this).val(1);
                        else
                            $(this).val(0);
                    });
                    $('#search_form_button').live('click', function() {
                        var queryParams = {};
                        $('.s_form_wrapper input[name]').filter(function(index) {
                            return $(this).val() != '';
                        }).each(function(i) {
                            if ($(this).val() != '')
                                queryParams[$(this).attr('name')] = $(this).val();
                        });
                        queryParams['search_form'] = true;
                        self.setData(datasource, queryParams);
                        // self.reload();
                        $('#operate_window').omWindow('close').empty();
                    });
                }
                if (toolbars['delete']) {
                    var optText = toolbars['delete']['text'],
                        optUrl = toolbars['delete']['href'],
                        delete_btn = buildButton(optText, 'icon-cancel').appendTo(toolbarF),
                        optParamName = toolbars['delete']['paramName'] || 'id';
                    delete_btn.after('<div class="datagrid-btn-separator" />');
                    delete_btn.bind('click', {
                        paramName: optParamName,
                        idValue: self.options.idField,
                        title: optText,
                        url: optUrl
                    }, function(e) {
                        $.omMessageBox.confirm({
                            title: e.data.title,
                            content: '确定要删除?',
                            onClose: function(r) {
                                if (r) {
                                    var ids = [];
                                    var selectedRows = self.getSelections(true);
                                    for (var i = 0; i < selectedRows.length; i++) {
                                        ids.push(selectedRows[i][e.data.idValue]);
                                    }
                                    ids = ids.join(',');
                                    if (ids) {
                                        $.get(e.data.url + '/' /*+ e.data.paramName + '/' */ + ids, function(data) {
                                            if (data) {
                                                $.omMessageBox.alert({
                                                    title: '操作结果',
                                                    content: data['status'] === true ? '删除成功' : '删除失败',
                                                    type: 'success'
                                                });
                                                self.reload();
                                            }
                                        }, 'json');
                                    } else {
                                        $.omMessageBox.alert({
                                            title: '警告',
                                            content: '没有任何行被选中!',
                                            type: 'warning'
                                        });
                                    }
                                }
                            }
                        });
                    });
                }
                if (toolbars['multiple']) {
                    var optText = toolbars['multiple']['text'],
                        optUrl = toolbars['multiple']['href'],
                        multiple_btn = buildButton(optText, 'icon-edit').appendTo(toolbarF),
                        optWidth = toolbars['multiple']['width'],
                        optHeight = toolbars['multiple']['height'];
                    multiple_btn.after('<div class="datagrid-btn-separator" />');
                    multiple_btn.bind('click', {
                        text: optText,
                        url: optUrl,
                        width: optWidth,
                        height: optHeight
                    }, function(e) {
                        buildWindow('operate_window', e.data.url, true).omWindow({
                            title: e.data.text,
                            width: e.data.width,
                            height: e.data.height,
                            collapsible: false,
                            maximizable: false,
                            minimizable: false,
                            modal: true,
                            onClose: function() {
                                // self.reload();
                            }
                        });
                        return false;
                    });
                    /*处理批量操作事件*/
                    $('.m_form_wrapper .check_row').live('click', function() {
                        if (this.checked)
                            $(this).val(1);
                        else
                            $(this).val(0);
                    });
                    $('#multiple_form_button').live('click', function() {
                        var queryParams = {};
                        $('.m_form_wrapper input[name]').filter(function(index) {
                            return $(this).val() != '';
                        }).each(function(i) {
                            queryParams[$(this).attr('name')] = $(this).val();
                        });
                        // console.log(queryParams);
                        queryParams['multiple_form'] = true;
                        var selectedRows = self.getSelections(true),
                            _selectedRows = [];
                        for (i = 0; i < selectedRows.length; i++) {
                            _selectedRows[i] = selectedRows[i][idField];
                        }
                        queryParams['batch_id'] = _selectedRows.join(',');
                        //     console.log(_selectedRows.join(','));
                        if (queryParams['batch_id']) {
                            $.omMessageBox.confirm({
                                title: '批量操作',
                                content: '确认操作?',
                                onClose: function(r) {
                                    if (r) {
                                        $.ajax({
                                            type: 'POST',
                                            url: optUrl,
                                            data: queryParams,
                                            dataType: 'json',
                                            success: function(data) {
                                                if (data.status == true) {
                                                    self.reload();
                                                    $('#operate_window').omWindow('close').empty();
                                                } else {
                                                    $.omMessageBox.alert({
                                                        title: '警告!',
                                                        content: 'no rows updateed'
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        } else {
                            $.omMessageBox.alert({
                                title: '警告',
                                content: '没有任何行被选中!',
                                type: 'warning'
                            });
                        }
                    });
                }
                if (toolbars['export']) {
                    var optText = toolbars['export']['text'],
                        export_btn = buildButton(optText, 'icon-export').appendTo(toolbarF),
                        export_href = toolbars['export']['href'],
                        export_width = toolbars['export']['width'],
                        export_height = toolbars['export']['height'],
                        export_menu = $('<div class="export_menu"></div>').appendTo(document.body).hide();
                    var _ids;
                    export_menu.omMenu({
                        minWidth: 130,
                        maxWidth: 230,
                        dataSource: [{
                            id: '1',
                            icon: 'icon-export',
                            label: '导出全部',
                            url: export_href,
                            seperator: true
                        }, {
                            id: '2',
                            icon: 'icon-export',
                            label: '导出已选择',
                            url: export_href
                        }],
                        onSelect: function(item, e) {
                            if (item.id == 2) {
                                var ids = [];
                                var selectedRows = self.getSelections(true);
                                for (var i = 0; i < selectedRows.length; i++) {
                                    ids.push(selectedRows[i][idField]);
                                }
                                _ids = ids.join(',');
                                if (!ids) {
                                    $.omMessageBox.alert({
                                        title: '警告',
                                        content: '没有任何行被选中!',
                                        type: 'warning'
                                    });
                                    return false;
                                }
                            }
                            buildWindow('operate_window', export_href, true).omWindow({
                                title: item.label,
                                width: export_width,
                                height: export_height,
                                collapsible: false,
                                maximizable: false,
                                minimizable: false,
                                modal: true,
                                onClose: function() {
                                    // self.reload();
                                }
                            });
                        }
                    });
                    /*单选钮点击事件*/
                    $('.export_form_wrapper .check_row').live('click', function() {
                        var column_name = $(this).attr('name');
                        if (this.checked)
                            $(this).val(column_name);
                        else
                            $(this).val('');
                    });
                    /*选择导出列点击事件*/
                    $('#export_form_button').live('click', function() {
                        var queryParams = {},
                            columns = [];
                        $.each($('.export_form_wrapper .check_row'), function(i, n) {
                            if ($(this).val() != '')
                                columns.push($(this).val());
                        });
                        queryParams['columns'] = columns.join(',');
                        queryParams['ids'] = _ids;
                        queryParams['export_form'] = true;
                        if (queryParams['columns']) {
                            $('#operate_window').omWindow('close');
                            $.ajax({
                                async: false,
                                type: 'POST',
                                url: export_href,
                                data: queryParams,
                                dataType: 'text',
                                beforeSend: function() {
                                    $.omMessageBox.waiting({
                                        title: '导出中',
                                        content: '服务器正在处理请求..'
                                    });
                                    //    return false;
                                },
                                success: function(file) {
                                    $.omMessageBox.waiting('close');
                                    $.omMessageBox.alert({
                                        type: 'success',
                                        title: '导出成功',
                                        content: '<div class="export_success"><a href="/export/down/' + file + '" target="_blank">下载保存</a></div>'
                                    })
                                    $('.om-messageBox-buttonset').parent().hide();
                                }
                            });
                        } else {
                            $.omMessageBox.alert({
                                title: '警告',
                                content: '还没有选择导出列!',
                                type: 'warning'
                            });
                        }
                    });
                    /*展开导出menu*/
                    export_btn.bind('click', function(e) {
                        export_menu.omMenu('show', this);
                    });
                }
                /*撤销重载grid*/
                if (toolbars['undo']) {
                    var undo_btn = buildButton(toolbars['undo']['text'], 'icon-undo').appendTo(toolbarF);
                    undo_btn.before('<div class="datagrid-btn-separator" />');
                    undo_btn.bind('click', function(e) {
                        window.location.reload();
                        return false;
                    });
                }
                self.titleDiv.after(toolbarDiv);
            });
        },
        _initColumns: function() {
            var self = this.el,
                cmp = this,
                idField = self.attr("idField"),
                initOperations = this._initOperations,
                cols = $("td").filter(function() {
                    return $("a", this).length == 0;
                }),
                optColumn = $("td[name='opt']", self),
                colData = [],
                optData = [];
            cols.each(function() {
                var colWidth = parseInt($(this).attr("width")),
                    colRenderer = $(this).attr('display');
                colItem = {
                    "name": $(this).attr("name"),
                    "header": $(this).text(),
                    "width": colWidth
                }
                if (colRenderer != undefined) {
                    colRenderFunction = new Function("v", "rowData", "rowIndex", colRenderer);
                    colItem["renderer"] = colRenderFunction;
                }
                $(this).attr("sort") != undefined && (colItem["sort"] = $(this).attr("sort"));
                colData.push(colItem);
            });


            $("a", optColumn).each(function() {
                var dialogWH = $(this).attr("dialog").split(","),
                    dialogReloadGrid = true;
                optData.push({
                    "text": $(this).text(),
                    "class": $(this).attr("class"),
                    "url": $(this).attr("href"),
                    "reload": dialogReloadGrid,
                    "width": dialogWH[0],
                    "height": dialogWH[1]
                });
            });
            optItem = {
                name: "opt",
                header: optColumn.attr("header"),
                width: parseInt(optColumn.attr("width")),
                align: optColumn.attr("align"),
                renderer: function(v, rowData, rowIndex) {
                    return initOperations(cmp, optData, rowData[idField], rowIndex);
                }
            }
            colData.push(optItem);
            this.colData = colData;
        },
        _initOperations: function(self, operations, idValue, index) {
            var opts = operations || {}, buildWindow = self._buildWindow;
            var operationsWrap = $('<div></div>').addClass('om-grid-operations');
            for (var i = 0, len = opts.length; i < len; i++) {
                var opt = opts[i],
                    optText = opt['text'],
                    optClass = opt['class'],
                    optUrl = opt['url'],
                    optReload = opt['reload'],
                    optWidth = opt['width'],
                    optHeight = opt['height'];
                var closeCallback = optReload ? function() {
                        self.el.omGrid('reload');
                    } : function() {};
                /*optParams = opt['params'] || {}, optParamName = opt['paramName'] || 'id';*/
                optUrl += /*'/' + optParamName +*/ '/' + idValue;
                var btn = $('<a href="javascript:void(0)" id="' + index + '_opts' + i + '" optWidth="' + optWidth + '" optHeight="' + optHeight + '" optUrl="' + optUrl + '" >' + optText + '</a>');
                btn.addClass(optClass);
                var btnId = btn.attr('id');
                $('#' + btnId).die('click');
                $('#' + index + '_opts' + i).live('click', function() {
                    buildWindow('operate_window', $(this).attr('optUrl')).omWindow({
                        title: $(this).text(),
                        width: $(this).attr('optWidth'),
                        height: $(this).attr('optHeight'),
                        modal: true,
                        onClose: closeCallback
                    });
                });
                operationsWrap.append(btn);
            }
            return operationsWrap.wrap('<div></div>').parent().html();
        }
    };
    module.exports = yauiGrid;
});
