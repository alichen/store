define('components/yaui-imglist', function(require, exports, module) {
    /*封装基于jquery-file-upload的商品图片上传组件*/
    var yauiImgList = {
        init: function(el) {
            var self = this,
                //el = $('.yaui-imglist', ctx),
                config = el.attr("yaui-options"),
                options = {
                    dataType: 'json',
                    dropZone: $('.img_district_name'),
                    pasteZone: $('.img_district_name'),
                    add: function(e, data) {
                        $.each(data.files, function(index, file) {
                            self.appendUploadItem(file, el);
                        });
                        data.submit()
                            .success(function(result, status) {
                                $.each(result.files, function(index, file) {
                                    self.resolveUploadItem(file, el);
                                });
                            })
                            .error(function(jqXHR, textStatus, errorThrown) {
                                if (errorThrown === 'abort') {
                                    alert('File Upload has been canceled');
                                }
                            });
                    }
                };
            config = $.parseJSON(config);
            $.extend(options, config);
            el.removeAttr('yaui-options').removeClass("yaui-imglist");
            require.async('vendor/jquery-file-upload', function() {
                el.find('.fileupload-input').fileupload(options);
            });
        },
        build: function(ctx) {
            var self = this,
                el = $('.yaui-imglist', ctx);
            el.each(function() {
                self.init($(this));
            });
        },
        appendUploadItem: function(file, el) {
            var name = file.name,
                rowList = $(el).next(),
                item = '<div><div class="upload_placeholder"><div class="upload_process"></div></div><p>' + name + '</p></div>';
            if (rowList.children('div').length > 9) {
                alert('max items is 10');
                return false;
            } else {
                if (rowList.is(':hidden')) {
                    rowList.show();
                }
                $(item).appendTo(rowList);
            }
        },
        resolveUploadItem: function(file, el) {
            var fileName = file.name,
                fileUrl = file.url,
                rowList = $(el).next(),
                galleryEl = $(el).find('#Gallery'),
                pathEl = $(el).find('#ImgPath'),
                imgEl = '<img alt="' + fileName + '" src="' + fileUrl + '/' + fileName + '"/>';
            rowList.children(':last').children('.upload_placeholder').replaceWith(imgEl);
            rowList.children(':last').children('p').text("副图");
            if (galleryEl.val() != '') {
                var gL = galleryEl.val().split(',');
                for (i = 0; i < gL.length; i++) {
                    if (gL[i] != fileName) {
                        gL.push(fileName);
                        break;
                    }
                }
                gL = gL.join(',');
                galleryEl.val(gL);
            } else {
                galleryEl.val(fileName);
            }
            if (pathEl.val() == '') {
                pathEl.val(filePath);
            }
        }
    };
    module.exports = yauiImgList;
});
