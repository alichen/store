define('components/yaui-mineditor', ['vendor/umeditor/umeditor.config', 'vendor/umeditor/umeditor'], function(require, exports, module) {
    window.UMEDITOR_HOME_URL = '/static/yaui/js/vendor/umeditor/';
    //var todo=require('vendor/todo');
    var yauiMineditor = {
        init: function(el) {
            var self = this,
                //el = $('.yaui-mineditor', ctx),
                config = el.attr("yaui-options"),
                URL = window.UMEDITOR_HOME_URL,
                options = {
                    //imageUrl:URL+"/mall",          //图片上传提交地址
                    imagePath: "", //图片修正地址，引用了fixedImagePath,如有特殊需求，可自行配置
                    //imageFieldName:"attachData",                  //图片数据的key,若此处修改，需要在后台对应文件修改对应参数
                    toolbar: [
                        'source | undo redo | bold italic underline strikethrough | superscript subscript | forecolor backcolor | removeformat |',
                        'insertorderedlist insertunorderedlist | selectall cleardoc paragraph | fontfamily fontsize',
                        '| justifyleft justifycenter justifyright justifyjustify |',
                        'link unlink | emotion image insertvideo  | map',
                        '| horizontal print preview fullscreen'
                    ],
                    initialFrameWidth: 810, //编辑器宽度
                    initialFrameHeight: 350, //编辑器高度
                    initialStyle: '.edui-editor-body .edui-body-container p{line-height:1em}', //编辑器层级的基数,可以用来改变字体等
                    autoHeightEnabled: false
                };
            config = $.parseJSON(config);
            $.extend(options, config);
            el.removeAttr('yaui-options').removeClass("yaui-mineditor");
            //console.log(module);
            // require('vendor/todo');
            require('vendor/umeditor/umeditor.config');
            require('vendor/umeditor/umeditor');
            UM.getEditor(el.attr('id'), options);
        },
        build: function(ctx) {
            var self = this,
                el = $('.yaui-mineditor', ctx);
            el.each(function() {
                self.init($(this));
            });
        }
    };
    module.exports = yauiMineditor;
});
