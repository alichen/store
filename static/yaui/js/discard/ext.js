(function($) {
    //  console.log($.yaui.lang._get("sidebar", "webshopAdmin"));
    //$(function() {
    //全局去掉虚线框
    $("a,input:not(input[type='text'],input[type='password'])").focus(function() {
        this.blur();
    });
   // });
    //回车键阻止表单提交
    $('#goods_form_wrap form input').keydown(function(event) {
        if(event.keyCode == 13) {
            return false;
        }
    });
    function debug(obj) {
        var res = "";
        $.each(obj, function(i, n) {
            res += i + ":" + (typeof(n) == "object" ? debug(n) : n) + ",\n";
        });
        return res;
    }

    $.extend($.yaui, {
        competents: {},
        init: function() {
            $.each(this.competents, function(name, competentObj) {
                var competentSelector = $("." + name);
                if (competentSelector.length > 0) {
                    competentSelector.each(function() {
                        competentObj.build(this);
                    })
                }
            });
        }
    });
    /*封装proxy组件*/
    $.extend($.yaui.competents,{
        "yaui-proxy":{
            build:function(el){
                var self=this,
                    type=$(el).attr("yaui-type"),
                   // value=$(el).val(),
                    options=$(el).attr("yaui-options")/*attrList=el.attributes,options={}*/;
                //不使用定义多个attribute的方式
                /*$.each(attrList,function(k,v){
                    if(v.nodeName.search(/^yaui-\w+/) >-1){
                        options[v.nodeName]=v.nodeValue;
                        $(self).removeAttr(v.nodeName);
                    }
                });*/
                options=JSON.parse(options);
               // $.extend(options,{value:value});
                $(el).removeClass('yaui-proxy').removeAttr('yaui-type').removeAttr('yaui-options');
                $.fn[type].call($(el),options);
            }
        }
    });
    /*封装omCombo组件*/
    $.extend($.yaui.competents, {
        "yaui-combo": {
            build: function(el) {
                var self = this.el = $(el),
                    config=$(el).attr("yaui-options"),
                    value=$(el).val(),
                    options={
                        inputField: 'Name',
                        valueField: 'Id',
                        optionField: 'Name',
                        value:value,
                       // listAutoWidth:true,
                        width: self.outerWidth(),
                        filterStrategy: 'anywhere'
                    };
                config=JSON.parse(config);
                $.extend(options,config);
                //dom清理
                self.removeAttr('yaui-options').removeClass('yaui-omcombo');
                self.omCombo(options);
            }
        }
    });
    //封装omComboTree组件
    $.extend($.yaui.competents, {
        "yaui-combotree": {
            build: function(el) {
                var self = this.el = $(el),
                    config=$(el).attr("yaui-options"),
                   // value=$(el).val(),
                    options = {
                        valueField: 'Id',
                       // value:value,
                        optionField: 'Name',
                        parentField:"ParentId",
                        simpleDataModel:true,
                       // lazyLoad: true,
                        collapseAll: true,
                        width: self.outerWidth(),
                    };
                config=JSON.parse(config);
                $.extend(options,config);
                //dom清理
                self.removeAttr('yaui-options').removeClass('yaui-omcombotree');
                self.omComboTree(options);
            }
        }
    });
    //封装omComboGrid组件
    $.extend($.yaui.competents, {
        "yaui-combogrid": {
            build: function(el) {
                var self = this.el = $(el),
                    config=$(el).attr("yaui-options"),
                    colModel=$.parseJSON($(el).attr("yaui-colmodel")),
                    //  grid每列有左右5px内间距,每列宽度加10px参与总宽度计算
                   // value=$(el).val(),
                    options = {
                        autoFit:false,
                        dataSource:'/brand/combo',
                        valueField: 'Id',
                        optionField: 'Name',
                        colModel:colModel,
                        limit:10,
                        method:'POST',
                        width:self.outerWidth(),
                        //width:210, //gird总宽度加20px(scroll宽度)
                        pageText:'第{index}页',
                        showIndex: false,
                       // listAutoWidth:true,
                        listMaxHeight:'auto',
                        //width:200,
                        gridWidth:200
                        //gridHeight:200,
                       // value:value,
                       // lazyLoad: true,
                        //width: self.outerWidth(),
                    };
                config=JSON.parse(config);
                $.extend(options,config);
                //dom清理
                self.removeAttr('yaui-options').removeClass('yaui-omcombotree');
                self.omComboGrid(options);
            }
        }
    });
    /*封装jQuery-File-Upload*/
    $.extend($.yaui.competents,{
        "yaui-fileupload":{
            build:function(el){
                var self=this,
                    config=$(el).attr("yaui-options"),
                options={
                    dataType: 'json',
                    done: function (e, data) {
                        $.each(data.result.files, function (index, file) {
                            $('<p/>').text(file.name).appendTo(document.body);
                        });
                    }
                };
                config=$.parseJSON(config);
                $.extend(options,config);
                $(el).removeAttr('yaui-options').removeClass("yaui-fileupload");
                $(el).fileupload(options);
            }
        }
    });
    /*封装ueditor组件*/
    $.extend($.yaui.competents, {
        "yaui-editor":{
            build:function(el){
                var self=this,
                    config=$(el).attr("yaui-options"),
                options={
                    initialFrameWidth:810, //编辑器宽度
                    initialFrameHeight:350, //编辑器高度
                    initialStyle:'p{line-height:1em}',//编辑器层级的基数,可以用来改变字体等
                    elementPathEnabled : false, //是否启用元素路径，默认是显示
                    autoHeightEnabled:false, // 是否自动长高,默认true
                    toolbarTopOffset:400, //编辑器底部距离工具栏高度(如果参数大于等于编辑器高度，则设置无效)
                };
                config=$.parseJSON(config);
                $.extend(options,config);
                $(el).removeAttr('yaui-options').removeClass("yaui-editor");
                UE.getEditor($(el).attr('id'),options);
            }
        }
    });
    /*封装omGrid组件*/
    $.extend($.yaui.competents, {
        "yaui-grid": {
            build: function(el) {
                var self = this.el = $(el),
                    cmp = this,
                    buildOpts;

                this._initColumns();
                this._initToolbar();
                buildOpts = {
                    title: self.attr("title"),
                    idField: self.attr("idField"),
                    autoFit: true,
                    limit: 19,
                    showIndex: false,
                    singleSelect: false,
                    height: 'auto',
                    method: 'POST',
                    toolbars: cmp.toolbars,
                    dataSource: self.attr("url"),
                    colModel: cmp.colData
                };
                //dom清理
                self.empty().removeAttr('url').removeAttr('idField').removeAttr('title').removeClass('yaui-omgrid');
                self.omGrid(buildOpts);
            },
            _buildWindow: function(id, href, isAjax, data) {
                $('#' + id).empty();
                var _isAjax = isAjax || false;
                var _data = data || {};
                if (href) {
                    if (_isAjax) {
                        $.ajax({
                            url: href,
                            dataType: 'html',
                            beforeSend: function() {
                                $.ajaxSetup({
                                    cache: true
                                });
                            },
                            complete: function() {
                                $.ajaxSetup({
                                    cache: false
                                });
                            },
                            data: _data,
                            success: function(data) {
                                var _filter = /<body[^>]*>((.|[\n\r])*)<\/body>/;
                                var _filter_body = _filter.exec(data);
                                $('#' + id).html(_filter_body[1]);
                            }
                        })
                    } else {
                        $('#' + id).css('padding', 0);
                        $('#' + id).html('<iframe marginheight=0 marginwidth=0 width="100%" height="100%" frameborder="0" border="0" scrolling="auto"></iframe>');
                        $('#' + id).children().data('src', href);
                        //    $('<iframe marginheight=0 marginwidth=0 width="100%" height="100%" frameborder="0" border="0" scrolling="yes" src="'+href+'"></iframe>').appendTo($('#'+id));
                    }
                }
                return $('#' + id);
            },
            _buildButton: function(text, iconCls) {
                var button = $("<div class='btn_wrap'><a href='#' class='" + iconCls + "'>" + text + "</a></div>");
                button.bind({
                    'mouseover': function() {
                        $(this).addClass('btn_hover');
                    },
                    'mouseout': function() {
                        $(this).removeClass('btn_hover');
                    }
                });
                //  return   button.omButton({width:80});
                return button;
            },
            _initToolbar: function() {
                var toolbars = {}, self = this.el,
                    toolbarsEl = $("th", self);
                toolbarsEl.each(function() {
                    toolbars[$("a", this).attr("name")] = {
                        "href": $("a", this).attr("href"),
                        "text": $("a", this).text()
                    }
                });
                this.toolbars = toolbars;

                this._injectToolbar();
            },
            /*注入omGrid组件的init调用*/
            _injectToolbar: function() {
                var buildWindow = this._buildWindow,
                    buildButton = this._buildButton;
                //var injectFunc = 

                $.omWidget.addInitListener('om.omGrid', function() {
                    var self = this,
                        datasource = self.options.dataSource,
                        idField = self.options.idField,
                        toolbars = self.options.toolbars,
                        queryParams;
                    if (!toolbars)
                        return;

                    /*加入工具栏*/
                    var toolbarDiv = $("<div class='toolbarDiv'></div>");
                    var toolbarF = $('<div class="toolbarRowF"></div>').appendTo(toolbarDiv);
                    if (toolbars['add']) {
                        var optText = toolbars['add']['text'],
                            optUrl = toolbars['add']['href'],
                            add_btn = buildButton(optText, 'icon-add').appendTo(toolbarF),
                            optWidth = toolbars['add']['width'] || 940,
                            optHeight = toolbars['add']['height'] || 600;
                        add_btn.after('<div class="datagrid-btn-separator" />');
                        add_btn.bind('click', {
                            text: optText,
                            url: optUrl,
                            width: optWidth,
                            height: optHeight
                        }, function(e) {
                            buildWindow('operate_window', e.data.url).omDialog({
                                title: e.data.text,
                                width: e.data.width,
                                height: e.data.height,
                                collapsible: true,
                                closeOnEscape: true,
                                modal: true,
                                onClose: function() {
                                    self.reload();
                                }
                            });
                            return false;
                        });
                    }
                    if (toolbars['search']) {
                        var optText = toolbars['search']['text'],
                            optUrl = toolbars['search']['href'],
                            search_btn = buildButton(optText, 'icon-search').appendTo(toolbarF),
                            optWidth = toolbars.search.width || 400,
                            optHeight = toolbars.search.height || 300;
                        search_btn.after('<div class="datagrid-btn-separator" />');
                        search_btn.bind('click', {
                            text: optText,
                            url: optUrl,
                            width: optWidth,
                            height: optHeight
                        }, function(e) {
                            buildWindow('operate_window', e.data.url, true).omDialog({
                                title: e.data.text,
                                width: e.data.width,
                                height: e.data.height,
                                collapsible: false,
                                modal: true,
                                onClose: function() {
                                    self.reload();
                                }
                            });
                            return false;
                        });
                        /*处理搜索事件*/
                        $('.s_form_wrapper .check_row').live('click', function() {
                            if (this.checked)
                                $(this).val(1);
                            else
                                $(this).val(0);
                        });
                        $('#search_form_button').live('click', function() {
                            var queryParams = {};
                            $('.s_form_wrapper .s_row').filter(function(index) {
                                return $(this).val() != '';
                            }).each(function(i) {
                                if ($(this).val() != '')
                                    queryParams[$(this).attr('name')] = $(this).val();
                            });
                            queryParams['search_form'] = true;
                            self.setData(datasource, queryParams);
                            $('#operate_window').omDialog('close').empty();
                        });
                    }
                    if (toolbars['delete']) {
                        var optText = toolbars['delete']['text'],
                            optUrl = toolbars['delete']['href'],
                            delete_btn = buildButton(optText, 'icon-cancel').appendTo(toolbarF),
                            optParamName = toolbars['delete']['paramName'] || 'id';
                        delete_btn.after('<div class="datagrid-btn-separator" />');
                        delete_btn.bind('click', {
                            paramName: optParamName,
                            idValue: self.options.idField,
                            title: optText,
                            url: optUrl
                        }, function(e) {
                            $.omMessageBox.confirm({
                                title: e.data.title,
                                content: '确定要删除?',
                                onClose: function(r) {
                                    if (r) {
                                        var ids = [];
                                        var selectedRows = self.getSelections(true);
                                        for (var i = 0; i < selectedRows.length; i++) {
                                            ids.push(selectedRows[i][e.data.idValue]);
                                        }
                                        ids = ids.join(',');
                                        if (ids) {
                                            $.get(e.data.url + '/' + e.data.paramName + '/' + ids, function(data) {
                                                if (data) {
                                                    self.reload();
                                                    $.omMessageBox.alert({
                                                        title: '操作结果',
                                                        content: data > 0 ? '删除成功' : '删除失败',
                                                        type: 'success'
                                                    });
                                                }
                                            }, 'text');
                                        } else {
                                            $.omMessageBox.alert({
                                                title: '警告',
                                                content: '没有任何行被选中!',
                                                type: 'warning'
                                            });
                                        }
                                    }
                                }
                            });
                        });
                    }
                    if (toolbars['multiple']) {
                        var optText = toolbars['multiple']['text'],
                            optUrl = toolbars['multiple']['href'],
                            multiple_btn = buildButton(toolbars.multiple.text, 'icon-edit').appendTo(toolbarF),
                            optWidth = toolbars.multiple.width || 400,
                            optHeight = toolbars.multiple.height || 300;
                        multiple_btn.after('<div class="datagrid-btn-separator" />');
                        multiple_btn.bind('click', {
                            text: optText,
                            url: optUrl,
                            width: optWidth,
                            height: optHeight
                        }, function(e) {
                            buildWindow('operate_window', e.data.url, true).omDialog({
                                title: e.data.text,
                                width: e.data.width,
                                height: e.data.height,
                                collapsible: false,
                                modal: true,
                                onClose: function() {
                                    self.reload();
                                }
                            });
                            return false;
                        });
                        /*处理批量操作事件*/
                        $('.m_form_wrapper .check_row').live('click', function() {
                            if (this.checked)
                                $(this).val(1);
                            else
                                $(this).val(0);
                        });
                        $('#multiple_form_button').live('click', function() {
                            var queryParams = {};
                            $('.m_form_wrapper .s_row').filter(function(index) {
                                return $(this).val() != '';
                            }).each(function(i) {
                                queryParams[$(this).attr('name')] = $(this).val();
                            });
                            // console.log(queryParams);
                            queryParams['multiple_form'] = true;
                            var selectedRows = self.getSelections(true),
                                _selectedRows = [];
                            for (i = 0; i < selectedRows.length; i++) {
                                _selectedRows[i] = selectedRows[i][idField];
                            }
                            queryParams['batch_id'] = _selectedRows.join(',');
                            //     console.log(_selectedRows.join(','));
                            if (queryParams['batch_id']) {
                                $.omMessageBox.confirm({
                                    title: '批量操作',
                                    content: '确认操作?',
                                    onClose: function(r) {
                                        if (r) {
                                            $.ajax({
                                                type: 'POST',
                                                url: optUrl,
                                                data: queryParams,
                                                dataType: 'json',
                                                success: function(data) {
                                                    if (data.status == true) {
                                                        self.reload();
                                                        $('#operate_window').omDialog('close').empty();
                                                    } else {
                                                        $.omMessageBox.alert({
                                                            title: '警告!',
                                                            content: 'no rows updateed'
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                $.omMessageBox.alert({
                                    title: '警告',
                                    content: '没有任何行被选中!',
                                    type: 'warning'
                                });
                            }
                            //    $('#operate_window').omDialog('close').empty();
                        });
                    }
                    if (toolbars['export']) {
                        var optText = toolbars['export']['text'],
                            export_btn = buildButton(optText, 'icon-export').appendTo(toolbarF),
                            export_href = toolbars['export']['href'],
                            export_menu = $('<div class="export_menu"></div>').appendTo(document.body).hide();
                        export_menu.omMenu({
                            minWidth: 130,
                            maxWidth: 230,
                            dataSource: [{
                                id: '1',
                                icon: 'icon-export',
                                label: '导出全部',
                                url: export_href,
                                seperator: true
                            }, {
                                id: '2',
                                icon: 'icon-export',
                                label: '导出已选择',
                                url: export_href
                            }],
                            onSelect: function(item, e) {
                                if (item.id == 2) {
                                    var ids = [];
                                    var selectedRows = self.getSelections(true);
                                    for (var i = 0; i < selectedRows.length; i++) {
                                        ids.push(selectedRows[i][idField]);
                                    }
                                    ids = ids.join(',');
                                    if (!ids) {
                                        $.omMessageBox.alert({
                                            title: '警告',
                                            content: '没有任何行被选中!',
                                            type: 'warning'
                                        });
                                        return false;
                                    }
                                    queryParams = {};
                                    queryParams['ids'] = ids;
                                    export_href = export_href + '?ids=' + ids;
                                }
                                buildWindow('operate_window', export_href, true).omDialog({
                                    title: item.label,
                                    width: 400,
                                    height: 400,
                                    collapsible: false,
                                    modal: true
                                });
                            }
                        });
                        /*单选钮点击事件*/
                        $('.export_form_wrapper .check_row').live('click', function() {
                            var column_name = $(this).attr('alt');
                            if (this.checked)
                                $(this).val(column_name);
                            else
                                $(this).val('');
                        });
                        /*选择导出列点击事件*/
                        $('#export_form_button').live('click', function() {
                            var queryParams = {},
                                columns = [];
                            $.each($('.export_form_wrapper .check_row'), function(i, n) {
                                if ($(this).val() != '')
                                    columns.push($(this).val());
                            })
                            queryParams['columns'] = columns.join(',');
                            queryParams['ids'] = $('#export_ids').val();
                            queryParams['export_form'] = true;
                            if (queryParams['columns']) {
                                $('#operate_window').omDialog('close');
                                $.ajax({
                                    async: false,
                                    type: 'POST',
                                    url: export_href,
                                    data: queryParams,
                                    dataType: 'text',
                                    beforeSend: function() {
                                        $.omMessageBox.waiting({
                                            title: '导出中',
                                            content: '服务器正在处理请求..'
                                        });
                                        //    return false;
                                    },
                                    success: function(file) {
                                        $.omMessageBox.waiting('close');
                                        $.omMessageBox.alert({
                                            type: 'success',
                                            title: '导出成功',
                                            content: '<div class="export_success"><a href="/../' + file + '" target="_blank">下载保存</a></div>'
                                        })
                                        $('.om-messageBox-buttonset').parent().hide();
                                    }
                                });
                            } else {
                                $.omMessageBox.alert({
                                    title: '警告',
                                    content: '还没有选择导出列!',
                                    type: 'warning'
                                });
                            }
                        });
                        /*展开导出menu*/
                        export_btn.bind('click', function(e) {
                            export_menu.omMenu('show', this);
                        });
                    }
                    /*撤销重载grid*/
                    if (toolbars['undo']) {
                        var undo_btn = buildButton(toolbars['undo']['text'], 'icon-undo').appendTo(toolbarF);
                        undo_btn.after('<div class="datagrid-btn-separator" />');
                        undo_btn.bind('click', function(e) {
                            window.location.reload();
                            return false;
                        });
                    }
                    self.titleDiv.after(toolbarDiv);
                });
            },
            _initColumns: function() {
                var self = this.el,
                    cmp = this,
                    idField = self.attr("idField"),
                    initOperations = this._initOperations,
                    cols = $("td").filter(function() {
                        return $("a", this).length == 0;
                    }),
                    optColumn = $("td[name='opt']", self),
                    colData = [],
                    optData = [];
                cols.each(function() {
                    var colWidth = parseInt($(this).attr("width"));
                    colItem = {
                        "name": $(this).attr("name"),
                        "header": $(this).text(),
                        "width": colWidth
                    }
                    $(this).attr("sort") != undefined && (colItem["sort"] = $(this).attr("sort"));
                    colData.push(colItem);
                });


                $("a", optColumn).each(function() {
                    var dialogWH = $(this).attr("dialog").split(",");
                    optData.push({
                        "text": $(this).text(),
                        "class": $(this).attr("class"),
                        "url": $(this).attr("href"),
                        "width": dialogWH[0],
                        "height": dialogWH[1]
                    });
                });
                optItem = {
                    name: "opt",
                    header: optColumn.attr("header"),
                    width: parseInt(optColumn.attr("width")),
                    align: optColumn.attr("align"),
                    renderer: function(v, rowData, rowIndex) {
                        return initOperations(optData, rowData[idField], rowIndex, cmp._buildWindow);
                    }
                }
                colData.push(optItem);
                this.colData = colData;
            },
            _initOperations: function(operations, idValue, index, buildWindow) {
                var opts = operations || {};
                var operationsWrap = $('<div></div>').addClass('om-grid-operations');
                for (var i = 0, len = opts.length; i < len; i++) {
                    var opt = opts[i],
                        optText = opt['text'],
                        optClass = opt['class'],
                        optUrl = opt['url'],
                        optWidth = opt['width']
                        optHeight = opt['height']
                        optParams = opt['params'] || {}, optParamName = opt['paramName'] || 'id';
                    optUrl += '/' + optParamName + '/' + idValue;
                    var btn = $('<a href="javascript:void(0)" id="' + index + '_opts' + i + '" optWidth="' + optWidth + '" optHeight="' + optHeight + '" optUrl="' + optUrl + '" >' + optText + '</a>');
                    btn.addClass(optClass);
                    var btnId = btn.attr('id');
                    $('#' + btnId).die('click');
                    $('#' + index + '_opts' + i).live('click', function() {
                        buildWindow('operate_window', $(this).attr('optUrl')).omDialog({
                            title: $(this).text(),
                            width: $(this).attr('optWidth'),
                            height: $(this).attr('optHeight'),
                            modal: true,
                            onClose: function() {
                                $('#grid_wrap').omGrid('reload');
                            }
                        });
                    });
                    operationsWrap.append(btn);
                }
                return operationsWrap.wrap('<div></div>').parent().html();
            }
        }
    })
    $(document).ready(function(){
        $.yaui.init();
    });
})(jQuery);
