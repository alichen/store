define('modules/yauiModule-authNode', function(require, exports, module) {
    var lang = require('lang');
    var authNode = {
        bindEvent: function() {
            var searchBtn = this.el.find('#search-nodes'),
                self = this,
                form = this.el.children('form'),
                url = this.el.find('form').attr('action'),
                treeEl = this.el.find('.yaui-tree');
            searchBtn.bind('click', function() {
                var groupId = self.el.find('input[name="gid"]').val();
                if (groupId === "") {
                    $.omMessageBox.alert({
                        type: 'error',
                        title: lang.cmpts['Empty'],
                        content: lang.cmpts['Its Empty']
                    });
                    return false;
                };
                url = url.replace('addaccess', 'authorize');
                $.get(url + '/' + groupId, function(data) {
                    treeEl.omTree('setData', data);
                });
            });
            form.bind('submit', function(e) {
                var selectedValues = treeEl.omTree('getChecked', true);
                var ids = [];
                for (var i = 0; i < selectedValues.length; i++) {
                    if (selectedValues[i].children) {
                        continue;
                    }
                    ids.push(selectedValues[i].id);
                }
                treeEl.next().val(ids.join(','));
                return true;
            });
        },
        create: function(el) {
            this.el = el;
            this.bindEvent();
        },
        init: function(ctx) {
            var self = this,
                el = $('.yauiModule-authNode', ctx);
            el.each(function() {
                self.create($(this));
            });
        }
    };
    module.exports = authNode;
});
