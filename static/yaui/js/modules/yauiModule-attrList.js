define('modules/yauiModule-attrList', function(require, exports, module) {
    var lang = require('lang');
    var attrList = {
        _initOpts: function() {
            var wrapEl = this.wrapEl;
            $('.item-del').die('click').live('click', function() {
                var curRow = $('.row_list_table tbody tr', wrapEl).find('td').has(this).parent();
                var curAttrId = curRow.find('.attr_name_row input').val();
                var newAttrs = $('#attr_handler ul#new-attrs li'),
                    oldAttrs = $('#attr_handler ul#old-attrs li');
                oldAttrs.each(function(index) {
                    if ($(this).children('input').val() == curAttrId) {
                        $(this).clone().appendTo($('#new-attrs'));
                        $(this).remove();
                    }
                });
                if ($('.row_list_table tbody tr', wrapEl).length != 1) {
                    curRow.empty().remove();
                }
            });
            $('.row_table_tr select').live('change', function() {
                var sVal = this.value;
            });
        },
        _appendItem: function() {
            var wrapEl = this.wrapEl;
            var lH = $('.row_list_table_first').html();
            lH = '<tr class="row_table_tr">' + lH + '</tr>';
            $('.row_list_table tbody', wrapEl).append(lH);
        },
        _initToolbar: function() {
            var self = this,
                wrapEl = this.wrapEl,
                parentEl = $(".row_table_list_tool", wrapEl);
            $('#add-attr', parentEl).click(function() {
                var attrHandler = $(this).next('#attr_handler'),
                    newAttrs = attrHandler.children('#new-attrs'),
                    oldAttrs = attrHandler.children('#old-attrs');
                if (newAttrs.children('li').length == 0) {
                    $.omMessageBox.alert({
                        type: 'error',
                        title: lang.cmpts['Add Failed'],
                        content: lang.cmpts['No NewAttr']
                    });
                    return;
                }
                var handlerPos = $(this).offset();
                attrHandler.css({
                    top: handlerPos.top + $(this).outerHeight() + 1,
                    left: handlerPos.left
                });
                attrHandler.toggle();
            });
            $('#attr_hanlder_btn').bind('click', function() {
                var addTpl = $('.row_list_table_first'),
                    newAttrs = $('#attr_handler ul#new-attrs li'),
                    newAttr = $(this).prev('#new-attrs').find('input:checked'),
                    attrId = newAttr.val(),
                    attrName = newAttr.next('label').text();
                if (newAttr.length == 0) {
                    $.omMessageBox.alert({
                        type: 'error',
                        title: lang.cmpts['Save Failed'],
                        content: lang.cmpts['No Attr Selected']
                    });
                    return; //没有选择
                }
                newAttrs.each(function(index) {
                    if ($(this).children('input').val() == attrId) {
                        $(this).clone().appendTo($('#old-attrs'));
                        $(this).remove();
                    }
                });
                //console.log(attrId+':'+attrName);
                addTpl.find(".attr_name_row input").addClass('attr_name').val(attrId).prev().text(attrName);
                self._appendItem();
                $(this).parent().hide();
            });
            $('#save-attrs').click(function() {
                var attrs = $('#attr_list tr.row_table_tr td').find('input,select').serialize();
                if (attrs === '') {
                    $.omMessageBox.alert({
                        type: 'error',
                        title: lang.cmpts['Save Failed'],
                        content: lang.cmpts['No Attr']
                    });
                    return;
                }
                $.post('', attrs, function(data) {
                    if (data.status == 1) {
                        $.omMessageBox.alert({
                            type: 'success',
                            title: lang.cmpts['Save Success'],
                            content: data.error

                        });
                    } else {
                        $.omMessageBox.alert({
                            type: 'error',
                            title: lang.cmpts['Save Failed'],
                            content: data.error
                        });
                    }
                }, 'json');
            });
        },
        create: function(el) {
            this.wrapEl = el;
            this._initToolbar();
            this._initOpts();
        },
        init: function(ctx) {
            var self = this,
                el = $('.yauiModule-attrList', ctx);
            el.each(function() {
                self.create($(this));
            });
        }
    };
    module.exports = attrList;
});
