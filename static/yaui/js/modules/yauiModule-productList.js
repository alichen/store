define('modules/yauiModule-productList', function(require, exports, module) {
    var lang = require('lang');
    var productList = {
        _initOpts: function() {
            var wrapEl = this.wrapEl;
            $('.item_manage .item_del').die('click').live('click', function() {
                var curRow = wrapEl.find('#row_list_table tbody tr td').has(this).parent();
                if (wrapEl.find('#row_list_table tbody tr').length != 1) {
                    curRow.empty().remove();
                }
            });
            $('.function_column').bind('contextmenu', function(e) {
                $(this).addClass('selected');
                var columnHandler = $('<div class="column_handler"></div>')
                    .appendTo($(this));
                columnHtml = '<a href="#" title="Move Toward" class="column_toward"><i class="fa fa-arrow-left fa-lg"></i> <b>' + lang.cmpts["Move Foward"] + '</b></a>';
                columnHtml += '<a href="#" title="Move Back" class="column_back"><i class="fa fa-arrow-right fa-lg"></i> <b>' + lang.cmpts["Move Back"] + '</b></a>';
                columnHtml += '<a href="#" title="Delete Column" class="column_del"><i class="fa fa-times fa-lg"></i> <b>' + lang.cmpts["Delete Column"] + '</b></a>';
                columnHandler.html(columnHtml);
                var columnPos = $(this).offset();
                columnHandler.css({
                    top: e.clientY,
                    left: e.clientX
                });
                e.preventDefault();
            }).mouseleave(function() {
                $(this).removeClass('selected');
                $(this).find('.column_handler').empty().remove();
            });
            $('.column_toward').die('click').live('click', function() {
                var curTh = $(this).parents('th'),
                    curTd = wrapEl.find('#row_list_table tbody tr td:eq(' + curTh.index() + ')');
                $(this).parent().empty().remove();
                if (curTh.index() != 0) {
                    curTh.prev('th').before(curTh);
                    curTd.each(function() {
                        $(this).prev('td').before($(this));
                    });
                } else {
                    $.omMessageBox.alert({
                        type: 'error',
                        title: lang.cmpts['Move Failed'],
                        content: lang.cmpts['Already First']
                    });
                }
            });
            $('.column_back').die('click').live('click', function() {
                var curTh = $(this).parents('th'),
                    curTd = wrapEl.find('#row_list_table tbody tr td:eq(' + curTh.index() + ')'),
                    thLength;
                $(this).parent().empty().remove();
                thLength = wrapEl.find('#row_list_table thead tr th.spec_column').length;
                if (curTh.index() < --thLength) {
                    curTh.next('th').after(curTh);
                    curTd.each(function() {
                        $(this).next('td').after($(this));
                    });
                } else {
                    $.omMessageBox.alert({
                        type: 'error',
                        title: lang.cmpts['Move Failed'],
                        content: lang.cmpts['Already Last']
                    });
                }
            });
            $('.column_del').die('click').live('click', function() {
                var curTh = $(this).parents('th'),
                    axis = curTh.attr('axis'),
                    curTd = wrapEl.find('#row_list_table tbody tr td:eq(' + curTh.index() + ')');
                var newSpecs = $('#spec_handler ul#new-specs li'),
                    oldSpecs = $('#spec_handler ul#old-specs li');
                oldSpecs.each(function(index) {
                    if ($(this).children('input').val() == axis) {
                        $(this).clone().appendTo($('#new-specs'));
                        $(this).remove();
                    }
                });
                if (curTh.parent().find('.spec_column').length > 1) {
                    curTh.addClass('new-spec');
                    curTd.each(function() {
                        $(this).addClass('new-spec');
                    });
                } else {
                    $.omMessageBox.alert({
                        type: 'error',
                        title: lang.cmpts['Delete Failed'],
                        content: lang.cmpts["Its Empty"]
                    });
                }
            });
        },
        _initToolbar: function() {
            var self = this,
                wrapEl = this.wrapEl,
                parentEl = $("#row_table_list_tool", wrapEl);
            $('#add-product', parentEl).click(function() {
                self._appendItem('row_list_table');
            });
            $('#add-spec', parentEl).click(function() {
                var specHandler = $(this).next('#spec_handler'),
                    specLen = specHandler.find('ul#new-specs li').length;
                if (specLen == 0) {
                    $.omMessageBox.alert({
                        type: 'error',
                        title: lang.cmpts['Add Failed'],
                        content: lang.cmpts['No NewSpec']
                    });
                    return;
                }
                var specPos = $(this).offset();
                specHandler.css({
                    top: specPos.top + $(this).outerHeight() + 3,
                    left: specPos.left
                });
                specHandler.toggle();
            });
            $('#save-spec').click(function() {
                var self = this,
                    newSpecs = $(this).prev('#new-specs').find('input:checked');
                if (newSpecs.length == 0) {
                    return;
                }
                newSpecs.each(function() {
                    var specId = this.value,
                        specName = $(this).attr('alt'),
                        index,
                        specThs = $('#row_list_table thead tr th.spec_column');
                    $(this).parent().clone().appendTo($(self).next());
                    $(this).closest('#spec_handler').hide();
                    $(this).parent().remove();
                    specThs.each(function(i) {
                        var axis = $(this).attr('axis');
                        if (axis == specId) {
                            index = i;
                            $(this).removeClass('new-spec')
                        }
                    });
                    $('#row_list_table tbody tr').each(function() {
                        $(this).children('.spec_column').eq(index).removeClass('new-spec');
                    });
                });
            });
            $('#save-products').click(function() {
                var productsEl = $('#row_list_table tr.row_table_tr'),
                    specThs = $('#row_list_table thead th.spec_column'),
                    specLen = $('#row_list_table thead th.spec_column').not('.new-spec').length,
                    products = {
                        'specLen': specLen,
                        'specValues': [],
                        'shopPrice': [],
                        'productNum': [],
                        'productImg': []
                    };
                if (productsEl.length == 0) {
                    $.omMessageBox.alert({
                        type: 'error',
                        title: lang.cmpts['Save Failed'],
                        content: lang.cmpts['No product']
                    });
                    return;
                }
                productsEl.each(function(i) {
                    var specEl = $(this).find('.spec_column').not('.new-spec'),
                        productShopPrice = $(this).find('input[name="spec_shop_price"]').val(),
                        productNum = $(this).find('input[name="spec_good_number"]').val(),
                        productImg = $(this).find('input[name="spec_good_img"]').val(),
                        productSpec = [];
                    specEl.children('input,select').each(function(k) {
                        var specId = specThs.not('.new-spec').eq(k).attr('axis'),
                            specVal = specThs.not('.new-spec').eq(k).text();
                        if ($(this).val() != "") {
                            productSpec[k] = specId + '_' + specVal + ':' + $(this).val();
                        }
                    });
                    productSpec = productSpec.join(',');
                    products['specValues'][i] = productSpec;
                    products['shopPrice'][i] = productShopPrice;
                    products['productNum'][i] = productNum;
                    products['productImg'][i] = productImg;
                });
                // console.log(products);
                $.ajax({
                    type: "post",
                    // async:false,
                    url: "",
                    data: products,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == 1) {
                            $.omMessageBox.alert({
                                type: 'success',
                                title: lang.cmpts['Save Success'],
                                content: data.error
                            });
                        } else {
                            $.omMessageBox.alert({
                                type: 'error',
                                title: lang.cmpts['Save Failed'],
                                content: data.error
                            });
                        }
                    }
                });
            });
        },
        _appendItem: function() {
            var lH = $('.row_list_table_first').html(),
                wrapEl = this.wrapEl
                lH = '<tr class="row_table_tr">' + lH + '</tr>';
            wrapEl.find('#row_list_table tbody').append(lH);
        },
        _initImgList: function() {
            $('.select_assoc_image').live({
                'click': function() {
                    $(this).parent().css({
                        position: 'relative'
                    });
                    $(this).parents('tr').siblings().find('.good_img_list').hide();
                    $(this).next('.good_img_list').css({
                        'position': 'absolute',
                        'top': $(this).outerHeight() + 1,
                        'left': '-480px',
                        'z-index': 2000
                    }).toggle();
                }
            });
            $('.good_img_list').live('mouseleave', function() {
                $(this).hide();
            });
            $('.check_select_img').live('click', function() {
                var img_var = $(this).parent().prev().attr('alt');
                if (this.checked) {
                    $(this).closest('.good_img_list_wrap').prev().val(img_var);
                    $(this).closest("li").siblings().find('.check_select_img').attr('checked', false);
                } else {
                    $(this).closest('.good_img_list_wrap').prev().val('');
                }
            });
        },
        create: function(el) {
            this.wrapEl = el;
            this._initToolbar();
            this._initOpts();
            this._initImgList();
        },
        init: function(ctx) {
            var self = this,
                el = $('.yauiModule-productList', ctx);
            el.each(function() {
                self.create($(this));
            });
        }
    };
    module.exports = productList;
});
