define('yaui', function(require, exports, module) {
    var $ = require('jquery');
    var lang = require('lang');
    var yaui = {
        debug: true,
        components: {
            'yaui-grid': this.debug ? require('components/yaui-grid') : {},
            'yaui-mineditor': this.debug ? require('components/yaui-mineditor') : {},
            //'yaui-editor':this.debug?require('components/yaui-editor'):{},
            'yaui-tree': this.debug ? require('components/yaui-tree') : {},
            'yaui-itemselector': this.debug ? require('components/yaui-itemselector') : {},
            'yaui-imglist': this.debug ? require('components/yaui-imglist') : {}
        },
        //异步加载组件
        _loadComponent: function(name,ctx) {
            require.async('components/' + name, function(asyncComponent) {
                asyncComponent.build(ctx);
            });
            /*            switch(name){
                case 'yaui-grid':
                    require.async('modules/yaui-grid',function(asyncModule){
                        asyncModule.build(el);
                    });
                    break;
            }*/
        },
        init: function(ctx) {
            var self = this;
            require('vendor/omui/omui');
            require('vendor/omui/om-window');
            $.each(this.components, function(name, competentObj) {
                var componentselector = $('.' + name, ctx);
                if (componentselector.length == 0) {
                    return true;
                }
                if ($.isEmptyObject(competentObj)) {
                    //console.log('async module');
                    self._loadComponent(name,ctx);
                } else {
                    competentObj.build(ctx);
                }
            });
        },
        build: function(ctx) {
            this.init(ctx);
            this.fixDom();
        },
        fixDom: function() {
            $('#goods_form_wrap form input').keydown(function(event) {
                if (event.keyCode == 13) {
                    return false;
                }
            });
            $("a,input:not(input[type='text'],input[type='password'])").focus(function() {
                this.blur();
            });
        }
    };
    /*封装proxy组件*/
    $.extend(yaui.components, {
        "yaui-proxy": {
            init: function(el) {
                var self = this,
                    // el=$('.yaui-proxy',ctx),
                    type = el.attr("yaui-type"),
                    // value=$(el).val(),
                    options = el.attr("yaui-options") /*attrList=el.attributes,options={}*/ ;
                //不使用定义多个attribute的方式
                /*$.each(attrList,function(k,v){
                    if(v.nodeName.search(/^yaui-\w+/) >-1){
                        options[v.nodeName]=v.nodeValue;
                        $(self).removeAttr(v.nodeName);
                    }
                });*/
                options = $.parseJSON(options);
                // $.extend(options,{value:value});
                el.removeClass('yaui-proxy').removeAttr('yaui-type').removeAttr('yaui-options');
                $.fn[type].call(el, options);
            },
            build: function(ctx) {
                var self = this,
                    el = $('.yaui-proxy', ctx);
                el.each(function() {
                    self.init($(this));
                });
            }
        }
    });
    /*封装omCombo组件*/
    $.extend(yaui.components, {
        "yaui-combo": {
            init: function(el) {
                var self = this,
                    // el=$('.yaui-combo',ctx)
                    config = el.attr("yaui-options"),
                    value = el.val(),
                    options = {
                        inputField: 'Name',
                        valueField: 'Id',
                        optionField: 'Name',
                        value: value,
                        emptyText: lang.combo["Please Select"],
                        width: el.outerWidth(),
                        filterStrategy: 'anywhere'
                    };
                config = $.parseJSON(config);
                $.extend(options, config);
                //dom清理
                el.removeAttr('yaui-options').removeClass('yaui-omcombo');
                el.omCombo(options);
            },
            build: function(ctx) {
                var self = this,
                    el = $('.yaui-combo', ctx);
                el.each(function() {
                    //console.log($(this).attr('yaui-options'));
                    self.init($(this));
                });
            }
        }
    });
    //封装omComboTree组件
    $.extend(yaui.components, {
        "yaui-combotree": {
            init: function(el) {
                var self = this,
                    //el = $('.yaui-combotree', ctx),
                    config = el.attr("yaui-options"),
                    value = el.val(),
                    options = {
                        valueField: 'Id',
                        value: value,
                        optionField: 'Name',
                        parentField: "ParentId",
                        simpleDataModel: true,
                        emptyText: lang.combo["Please Select"],
                        // lazyLoad: true,
                        collapseAll: true,
                        width: el.outerWidth(),
                    };
                config = $.parseJSON(config);
                $.extend(options, config);
                //dom清理
                el.removeAttr('yaui-options').removeClass('yaui-omcombotree');
                el.omComboTree(options);
            },
            build: function(ctx) {
                var self = this,
                    el = $('.yaui-combotree', ctx);
                el.each(function() {
                    self.init($(this));
                });
            }
        }
    });
    //封装omComboGrid组件
    $.extend(yaui.components, {
        "yaui-combogrid": {
            init: function(el) {
                var self = this,
                    //el = $('.yaui-combogrid', ctx),
                    config = el.attr("yaui-options"),
                    colModel = $.parseJSON(el.attr("yaui-colmodel")),
                    //  grid每列有左右5px内间距,每列宽度加10px参与总宽度计算
                    val = el.val(),
                    text = el.attr('alt'),
                    value = (val == "" ? "" : [{
                        Id: val,
                        Name: text
                    }]),
                    options = {
                        autoFit: false,
                        //dataSource:'/brand/combo',
                        valueField: 'Id',
                        optionField: 'Name',
                        colModel: colModel,
                        limit: 10,
                        method: 'POST',
                        width: el.outerWidth(),
                        //width:210, //gird总宽度加20px(scroll宽度)
                        pageText: '第{index}页',
                        showIndex: false,
                        emptyText: lang.combo["Please Select"],
                        listMaxHeight: 'auto',
                        //width:200,
                        gridWidth: 200,
                        //gridHeight:200,
                        value: value
                        // lazyLoad: true,
                        //width: self.outerWidth(),
                    };
                //console.log(value);
                config = $.parseJSON(config);
                $.extend(options, config);
                //dom清理
                el.removeAttr('yaui-options').removeClass('yaui-omcombotree');
                require("vendor/omui/om-combogrid");
                // require.async('vendor/omui/om-combogrid',function(){
                el.omComboGrid(options);
                //  });
            },
            build: function(ctx) {
                var self = this,
                    el = $('.yaui-combogrid', ctx);
                el.each(function() {
                    self.init($(this));
                });
            }
        }
    });
    /*封装jQuery-File-Upload*/
    $.extend(yaui.components, {
        "yaui-fileupload": {
            init: function(el) {
                var self = this,
                    el = $('.yaui-fileupload', ctx),
                    config = el.attr("yaui-options"),
                    options = {
                        dataType: 'json',
                        done: function(e, data) {
                            $.each(data.result.files, function(index, file) {
                                $('<p/>').text(file.name).appendTo(document.body);
                            });
                        }
                    };
                config = $.parseJSON(config);
                $.extend(options, config);
                el.removeAttr('yaui-options').removeClass("yaui-fileupload");
                el.fileupload(options);
            },
            build: function(ctx) {
                var self = this,
                    el = $('.yaui-fileupload', ctx);
                el.each(function() {
                    self.init($(this));
                });
            }
        }
    });
    $(document).ready(function() {
        yaui.build(document.body);
    });
    module.exports = yaui;
});