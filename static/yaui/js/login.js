define('login',function(require,exports,module){
	var $=require('jquery');
	$(document).ready(function(){
		$('#captcha_img>img').click(function(event) {
			var src=$(this).attr('src'),keyInput=$(this).parent().prev(':hidden'),self=this;
			$.get("/refresh/"+Math.random(),function(data){
				var newSrc=data.key;
				$(self).attr('src', '/verify/'+newSrc);
				keyInput.val(newSrc);
			},'json')
		});

		var lang=require('lang');
		require.async('vendor/omui/omui',function(){
		//	console.log(lang.validate.required)
			var loginValidator=$('#login-form').validate({
				rules:{
					Name:{
						required:true
					},
					Password:{
						required:true
					},
					Verify:{
						required:true
					}
				},
				messages:{
					Name:{
						required:lang.validate.required
					},
					Password:{
						required:lang.validate.required
					},
					Verify:{
						required:lang.validate.required
					}
				},
				//errorElement:'span',
				errorPlacement:function(err,input){
					//var msg=$(err).text();
					err.insertAfter(input.parent());
					//fa fa-times-circle fa-lg
				},
				showErrors:function(errorMap,errorList){
					if(errorList && errorList.length > 0){
						$.each(errorList,function(index,obj){
//							var msg = this.message;
							$(obj.element).parent().next().show();
						});
					}else{
						$(this.currentElements).parent().next('.error').hide();
					}
					this.defaultShowErrors();
				}
			});

			$('#login-form').submit(function(){
				var self=this;
				if(!$(this).valid()){
					return false;
				}
				$.ajax({
					type:'POST',
					url:$(this).attr('url'),
					data:$(this).serialize(),
					dataType:'json',
					success:function(resp,status,xhr){
						var isValid=resp.isValid;
						if(isValid){
							window.location.href="/";
						}else{
							loginValidator.showErrors(resp);
							return false;
						}
					}
				});
				return false;
			});
		});
	});
});