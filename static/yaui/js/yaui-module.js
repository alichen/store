define('yaui-module', function(require, exports, module) {
    var $ = require('jquery');
    require('vendor/omui/omui');
    var yauiModule = {
        debug: true,
        modules: {
            'yauiModule-productList': this.debug ? require('modules/yauiModule-productList') : {},
            'yauiModule-attrList': this.debug ? require('modules/yauiModule-attrList') : {},
            'yauiModule-authNode': this.debug ? require('modules/yauiModule-authNode') : {},
        },
        //异步加载组件
        _loadModule: function(name,ctx) {
            require.async('modules/' + name, function(asyncModule) {
                asyncModule.init(ctx);
            });
        },
        build: function(ctx) {
            var self = this;
            $.each(this.modules, function(name, moduleObj) {
                var moduleselector = $('.' + name, ctx);
                if (moduleselector.length == 0) {
                    return true;
                }
                if ($.isEmptyObject(moduleObj)) {
                    //console.log('async module');
                    self._loadModule(name,ctx);
                } else {
                    moduleObj.init(ctx);
                }
            });
        }
    };
    $(document).ready(function() {
        yauiModule.build(document.body);
    });
});
