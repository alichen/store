1.om-core
2.om-mouse->om-core.js
3.om-position
4.om-draggable->om-core.js,om-mouse.js
5.om-droppable->om-core.js,om-mouse.js,om-draggable.js
6.om-resizable->om-core.js,om-mouse.js
7.om-sortable->om-core.js,om-mouse.js
8.om-panel->om-core.js
9.om-grid->om-core.js,om-mouse.js,om-resizable.js
10.om-grid-headergroup->om-grid.js
11.om-grid-roweditor->om-grid.js,om-button.js
12.om-grid-rowexpander->om-grid.js
13.om-grid-sort->om-grid.js
14.om-accordion->om-core.js,om-panel.js
15.om-ajaxsubmit
16.om-borderlayout->om-core.js,om-mouse.js,om-resizable.js,om-panel.js
17.om-button->om-core.js
18.om-buttonbar->om-core.js /
19.om-calendar
20.om-combo->om-core.js
21.om-dialog->om-core.js,om-button.js,om-draggable.js,om-mouse.js,om-position.js,om-resizable.js
22.om-fileupload
23.om-itemselector->om-core.js,om-mouse.js,om-sortable.js
24.om-menu->om-core.js
25.om-messagebox->om-core.js,om-mouse.js,om-draggable.js,om-position.js
26.om-messagetip->om-core.js
27.om-numberfield->om-core.js
28.om-progressbar->om-core.js
29.om-scrollbar->om-core.js /
30.om-slider.js->om-core.js /
31.om-suggestion->om-core.js
32.om-tabs->om-panel.js
33.om-tooltip->om-core.js
34.om-tree->om-core.js
35.om-validate


om-calendar.js 修改
-----------------------------------------------------------------------
1.0修改办法
280行加上 .css({position:'relative'})
282行由document.body 改成 $self.parent()
show 方法中
注释掉
// this.con.css('left', _x.toString() + 'px');
// this.con.css('top', _y.toString() + 'px');
this.con.css('left', '-1px');
this.con.css('top', $container.outerHeight());
465行由 _y = $container.offset().top + height; 改成 _y=height;
466行由  _x.toString() +'px' 改成 '-1px'
时间选择构造器 以及 Page
984行,985行加入 type="button"
380行.show() 改为 .toggle()

_buildStatusEvent方法中加入
.parent().unbind().bind('mouseleave',function(){
  self.hide();
})

om-panel.js 修改
 -----------------------------------------------------------------------
 1.0修改办法
 332行,334行,342行注释掉
 488行加入
  if(this.options.header==false){
        $body.height('100%');
 }else{
        $body.outerHeight($panel.height()- $header.outerHeight() );
 }
 -----------------------------------------------------------------------
 2.0修改办法
 init 方法中
 注释掉 335行
_resize 方法中
加入
if(options.header==false){
    $body.height('100%');
}else{
    $body.outerHeight($panel.height()- (this.options.header!==false?$header.outerHeight():0) );
}

om-tabs.js 修改
-------------------------------------------------------------------------
2.1修改
_renderBody 方法中去掉
var contentPos = {};
 contentPos.height = $panels.height(); 
if(options.width !== 'auto'){
  contentPos.width = $panels.width();
}

.omPanel("resize" , contentPos)

_add 方法中去掉
var contentPos = {},
$tabPanels = $self.find('>div.om-tabs-panels');
if(options.width !== 'auto'){
  contentPos.width = $tabPanels.width();
}
if(options.height !== 'auto'){
  contentPos.height = $tabPanels.height();
}

$.extend(cfg, config /*contentPos*/);
 585行,1047行加入 height:'fit'
 765行,766行 注释掉
 775行加入
 $(window).bind('resize',function(){
      var omtabsHeight= $omtabs.parent().innerHeight(),
      headersHeight = $omtabs.find('>div.om-tabs-headers').outerHeight();
      $omtabs.height(omtabsHeight);
       $omtabs.find('>div.om-tabs-panels').height(omtabsHeight-headersHeight);
 });
 -------------------------------------------------------------------------
 2.0开始下面的修改废除
 1006行加入 
 else{
         rt=false;
         return true;
 }
1098行 由this._renderBody() 改为 this._renderPartial($nPanel);
1077行修改为
if (config.index == items.length) {
    items[config.index] = $nPanel;
    $nHeader.appendTo($ul);
    $nPanel.addClass("om-state-nobd").parent().appendTo($panels)
} else {
    //insert at index
    items.splice(config.index, 0, $nPanel);
    $ul.children().eq(config.index).before($nHeader);
    $panels.children().eq(config.index).before($nPanel);
}
1129行改为.empty().remove();
1139行加入
if(tabId === his[--len]){
    his.splice(len,1);
}else{
    $.each(his,function(i,n){
        if(n==tabId){
            his.splice(i,1);
        }
    });
}
注释掉
while(his[--len] && tabId === his[len]){
            his.splice(len , 1);
            break;
            }


om-grid 修改
--------------------------------------------------------------------------------
1.0修改如下
531行
//  var theadHeight = grid.children('.hDiv').outerHeight(),
//     pagingToolBarHeight = grid.children('.pDiv').outerHeight() || 0,
//     titleHeight = grid.children('.titleDiv').outerHeight() || 0,
var    tbody = grid.children('.bDiv');
//    tbody.height(grid.height()-theadHeight-pagingToolBarHeight-titleHeight);
tbody.height("auto");
this.pageData={nowPage:1,totalPages:1};
this._populate();
554行加入
$(window).bind('resize',function(){
    var tableWidth=grid.width()-20,
        usableWidth=tableWidth-self.hDiv.find('thead').width(),
        allColsWidth=0;
    for (var i=0,len=options.colModel.length;i<len;i++) {
        var cm=options.colModel[i],cmWidth = cm.width || 60;
        allColsWidth += cmWidth;
    }
    var percent=1+usableWidth/allColsWidth,
        toFixThs=self.hDiv.find('th[axis^="col"]'),
        toFixTds=self.hDiv.next().find('td[abbr!=""]');
    for (var i=0,len=options.colModel.length;i<len;i++) {
        var thCol=toFixThs.eq(i),thColWidth=parseInt(thCol.children().width()*percent);
        self.hDiv.children().css('width',self.hDiv.width()-40);
        self.hDiv.find('table').css('width',self.hDiv.width()-40);
        self.hDiv.next().find('table').css('width',self.hDiv.width()-40);
        thCol.children().css('width',thColWidth);

        self.hDiv.next().find('tr').each(function(j){
           $(this).children('td[abbr!=""]').eq(i).children().css('width',thColWidth);
        });
    }
});
719行修改
height:grid.height() 改成 height:'100%'
---------------------------------------------
2.0修改如下
_resetHeight 方法中
注释掉
var headerHeight = this.hDiv.outerHeight(true),
    pagerHeight = this.pDiv.is(":hidden")? 0 : this.pDiv.outerHeight(true),
    titleHeight = this.titleDiv.is(":hidden")? 0 : this.titleDiv.outerHeight(true);
以及
$grid.children('.bDiv').outerHeight($grid.height() - headerHeight - pagerHeight - titleHeight)
增加
$grid.children('.bDiv').height('auto');

_init 方法中
增加
$(window).bind('resize',function(){
    self.resize({width:'fit',height:'auto'});
});

_buildLoadMask 方法中
改为 this.loadMask.css({width:"100%",height:'100%'});

setData 方法修改为
setData:function(url,extraData){
    this.options.dataSource=url;
    this.options.extraData=extraData || {};
    this.pageData={nowPage:1,totalPages:1};
    this._populate();
},


om-combo.js 修改
 515行加入 not(dropList) 665行和772行查找.show()去掉
 396行 由 document.body 改成 span
span 加入 position:relative;
 664行去掉 inputPos.left 和 inputPos.top,定位起点以span为准
 664行 改成 -1px,665行 改成 outerHeight()
673行 由 show() 改为 toggle()
_bindEventsToList 方法中加入
click(function(){ self._backfill(this);})

_create方法改为
var textInput = this.element;
var span = $('<span class="om-combo om-widget om-state-default"></span>').css('position','relative').insertAfter(textInput).wrapInner(textInput);
this.valueEl = $('<input type="hidden"/>').attr('name',textInput.attr('name')).prependTo(span);
this.textInput=textInput.removeAttr('name');
this.expandTrigger = $('<span class="om-combo-trigger"></span>').appendTo(span);
this.dropList = $($('<div class="om-widget"><div class="om-widget-content om-droplist"></div></div>').css({position:'absolute', zIndex:2000}).appendTo(span).children()[0]).hide();
if(this.options['pager']){
  var pager=$('<div class="pager btn"><a class="prev">&lt;</a><input type="text" value="0"><a class="next">&gt;</a></div>').insertAfter(this.dropList).hide();
  this.pager=pager;
}

所有 valueEl=this.element 改为valueEl=this.valueEl


_loadData 方法后面改为
if (innerHtml) {
    $(innerHtml).appendTo(dropList);
    if (options.pager){
      self.pager.hide();
    }
    dropList.hide();
    self._initDropList();
    this._bindEventsToList();
}
_setValue 方法中增加 valueEl.val(value);
_selectNext 方法中去掉 this.showDropList()

_dropListToggle:function(){
  if(this.dropList.is(':hidden')){
    this.dropList.show();
    if(this.options.pager){
      this.pager.show();
    }
  }else{
    this.dropList.hide();
    if(this.options.pager){
      this.pager.hide();
    }
  }
},
_buildEvent 方法中的 focus this.showDropList() 改为 self._dropListToggle();

_buildEvent 方法中
focus 事件中增加
self._togglePager();

keyup 事件中Escape中
if (options.pager){
  self.pager.hide();
}


$(document).bind('mousedown.omCombo',this.globalEvent=function(){
    dropList.hide();
    if(options.pager){
      self.pager.hide();
    }
});

_backfill 方法中增加
if (options.pager){
  self.pager.hide();
}

om-menu.js
show 方法中
142行加入 'z-index':1000 "top":top+1

_appendNodes 方法中
276行,289行由
item.icon?childrenHtml.push("<img class=\""+imgClass+item.icon+"\"></span>"):null;
改为
item.icon?childrenHtml.push("<span class=\""+imgClass+item.icon+"\"></span>"):null;

om-tree.js
_init 方法中
438行增加 options.onSuccess && self._trigger("onSuccess",null,source);

om-accordion.js
_create 方法中
422行加入
$(window).bind('resize',function(){
    var panels= $.data($acc , "panels"),headerHeight = 0;
        $.each(panels , function(index , panel){
        headerHeight += panel.prev().outerHeight();
    });
    $.each(panels , function(index , panel){
            if(options.height === 'auto'){
                    panel.css('height'  , "");
            }
            if(options.height === 'fit'){
                    panel.outerHeight($acc.height() - headerHeight);
            }
    });
});
530行,534行,539行,336行
 .prop 方法 换成 .attr(2.0版本废除)

 om-dialog.js
-------------------------------------------------------
2.0 修改办法
_create 方法中
 270行加入
 self.element
     .show()
     .removeAttr('title')
     .addClass(
     'om-dialog-content ' +
         'om-widget-content').wrap('<div></div>');

去掉 uiDialogContent定义
原来的self.uiDialog = $('<div></div>') 变更为 self.uiDialog = self.element.parent()
_init行加入
if(this.element.find('iframe').length>0){
     var iframeEl=this.element.find('iframe'),iframeElSrc=iframeEl.data('src');
     iframeEl.attr('src',iframeElSrc);
     iframeEl.removeData('src');
  //   iframeEl.removeAttr('loadsrc');
}