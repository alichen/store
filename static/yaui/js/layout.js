define('layout',function(require,exports,module){
    var $=require('jquery');
    $(document).ready(function() {
        var lang=require('lang');
       // require('vendor/omui/omui');
        require.async('vendor/omui/omui',function(){
            //全局layout
            $('body').omBorderLayout({
                panels: [{
                    id: 'home-header',
                    header: false,
                    region: 'north',
                    resizable: false,
                    collapsible: false
                }, {
                    id: 'home-content',
                    header: false,
                    region: 'center'
                }, {
                    id: 'home-sidebar',
                    title: lang.layout.mainMenu,
                    region: 'west',
                    resizable: true,
                    collapsible: true,
                    width: 160
                }, {
                    id: 'home-footer',
                    header: false,
                    region: 'south',
                    resizable: false,
                    collapsible: true
                }],
                spacing: 3
            });
            //侧边栏渲染
            var OmuiSidebar = {
                build: function() {
                    var sidebarRowsUrl = $("div#sidebar_wrap").attr("sidebarUrl") || "/sidebar",
                        self = this;
                    $.getJSON(sidebarRowsUrl, function(sidebarRows) {
                        self.initData(sidebarRows).initDom().initTabs().initAccordion().initNavTree();
                    });
                },
                initData: function(sidebarRows) {
                    var sidebarTitles = [],
                        sidebarItems = {};
                    $.each(sidebarRows, function(i, n) {
                        sidebarTitles.push(n.name);
                        sidebarItems[n.name] = n.items
                    });
                    this.sidebarTitles = sidebarTitles;
                    this.sidebarItems = sidebarItems;
                    return this;
                },
                initDom: function() {
                    var siderBarInner = $("<ul></ul").appendTo("div#sidebar_wrap");
                    $.each(this.sidebarTitles, function(k, n) {
                        siderBarInner.append('<li><a href="#' + n + '">' + lang._get("sidebar", n) + '</a></li>');
                        siderBarInner.after('<div id="' + n + '"><ul class="sidebar_tree"></ul></div>');
                    });
                    return this;
                },
                initTabs: function() {
                    this.tabs = $('#iframe_wrap').omTabs({
                        closable: true,
                        border: false,
                        height: 'fit'
                    });
                    return this;
                },
                initAccordion: function() {
                    $('#sidebar_wrap').omAccordion({
                        switchEffect: true,
                        height: 'fit'
                    });
                    return this;
                },
                initNavTree: function() {
                    var el = $('.sidebar_tree'),
                        data = this.sidebarItems
                        tabs = this.tabs;
                    el.each(function(i) {
                        var index = $(this).parent().attr('id'),
                            // var ds = eval('sidebarRows.' + index);
                            _ds = data[index]
                            ds = [];
                        $("a", el).focus(function() {
                            this.blur()
                        }); //去掉虚线边框
                        //由于serveJson转换问题,生成了string原始值,而非Json对象
                        /*                    $.each(ds,function(i,n){
                            eval("var theJsonValue = "+n);
                            ds[i]=theJsonValue;
                        });*/
                        $.each(_ds, function(i, row) {
                            var _row = {};
                            rowArr = row.split(',');
                            $.each(rowArr, function(k, v) {
                                _v = v.split(':');
                                _row[_v[0]] = _v[1];
                            });
                            ds.push(_row);
                        });
                        //console.log(ds);
                        $(this).omTree({
                            dataSource: ds,
                            simpleDataModel: true,
                            onSuccess: function(data, e) {
                                $(this).omTree('expandAll');
                            },
                            onClick: function(node, e) {
                                if (node.url != '') {
                                    if (tabs.omTabs('getAlter', 'om-tabs-' + node.id) == null) {
                                        tabs.omTabs('add', {
                                            title: node.text,
                                            content: '<iframe scrolling="auto" style="overflow-x:auto;" width="100%" height="100%" marginheight=0 marginwidth=0 frameborder=0 src="' + node.url + '"></iframe>',
                                            tabId: 'om-tabs-' + node.id,
                                            closable: true
                                        });
                                    } else {
                                        tabs.omTabs('activate', 'om-tabs-' + node.id);
                                    }
                                } else
                                    return false;
                            }
                        });
                        return true;
                    });
                }
            };
            OmuiSidebar.build();
        });
    });
});
