/*
 *扩展的i18n语言包
 */
 define(function(require,exports,module){
    var $=require('jquery');
    /*定义一些全局常量和方法*/
    //$.yaui = $.yaui || {};
    var lang={
        _get: function(comp, attr) {
            return this[comp][attr];
        }
    };

    $.extend(
        lang, {
            validate:{
                'required':'不能为空'
            },
            layout: {
                mainMenu: "主菜单"
            },
            sidebar: {
                webshopAdmin: "商城管理",
                accessAdmin: "授权管理",
                storeAdmin:"存储管理",
                systemAdmin: "系统管理"
            },
            combo:{
                "Please Select":"请选择"
            },
            cmpts:{
                "Save Success":"保存成功",
                "Save Failed":"保存失败",
                "Add Failed":"添加失败",
                "No NewSpec":"无新规格",
                "No product":"无货品",
                "No Attr":"无属性",
                "No NewAttr":"无新属性",
                "No Attr Selected":"无任何属性被选择",
                "Move Foward":"前移",
                "Move Back":"后移",
                "Delete Column":"删除列",
                "Move Failed":"移动失败",
                "Already First":"已经最前",
                "Already Last":"已经最后",
                "Empty":"为空",
                "Its Empty":"值为空"
            }
        }
    );

    module.exports=lang;
});
